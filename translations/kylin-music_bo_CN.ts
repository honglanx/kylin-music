<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AllPupWindow</name>
    <message>
        <source>Prompt information</source>
        <translation>གསལ་འདེབས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>ཡི་གེ་འདུས་མི་ཐུབ་པ། / * &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <source>Reached upper character limit</source>
        <translation>སྟེང་གི་མི་སྣའི་ཚད་ལ་སླེབས་པ།</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation>རོལ་དབྱངས།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ངོས་འཛིན་</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>Please input playlist name:</source>
        <translation>མིང་ཐོ་འགོད་རོགས།</translation>
    </message>
</context>
<context>
    <name>CustomToolButton</name>
    <message>
        <source>Play</source>
        <translation type="vanished">རྩེད་མོ་རྩེ་བ།</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">བསུབ་པ།</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">གླུ་གཞས་རེའུ་མིག་</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation type="vanished">ང་དགའ་པོ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <source>Music Player</source>
        <translation type="vanished">རོལ་དབྱངས།</translation>
    </message>
    <message>
        <source>Loop</source>
        <translation type="vanished">རེའུ་མིག་རྒྱུན་འཁོར།</translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="vanished">སྐབས་བསྟུན་རང་བཞིན།</translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation type="vanished">གོ་རིམ།</translation>
    </message>
    <message>
        <source>00:00/00:00</source>
        <translation type="vanished">00:00/00:00</translation>
    </message>
    <message>
        <source>CurrentItemInLoop</source>
        <translation type="vanished">རྐྱང་འཁྱོག་རྒྱུན་འཁོར།</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation type="vanished">ང་དགའ་པོ་ཡོད།</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">རྩེད་མོ་རྩེ་བ།</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation type="obsolete">ཉར་ཚགས་ </translation>
    </message>
</context>
<context>
    <name>MusicDataBase</name>
    <message>
        <source>Prompt information</source>
        <translation type="obsolete">གསལ་འདེབས་ཆ་འཕྲིན། </translation>
    </message>
    <message>
        <source>Single song name is already exists</source>
        <translation type="obsolete">གཞས་ཀྱི་ཁེར་མིང་ཡོད། </translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <source>Unknown singer</source>
        <translation>མ་ཤེས་པའི་གླུ་བ་ </translation>
    </message>
    <message>
        <source>Add failed, no valid music file found</source>
        <translation>None </translation>
    </message>
    <message>
        <source>Prompt information</source>
        <translation>གསལ་འདེབས་ཆ་འཕྲིན། </translation>
    </message>
    <message>
        <source>Unknown album</source>
        <translation>མ་ཤེས་པའི་ཆེད་སྒྲིག་ </translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <source>Music Player</source>
        <translation>རོལ་དབྱངས། </translation>
    </message>
    <message>
        <source>File Type : </source>
        <translation>ཡིག་ཆའི་རིགས། </translation>
    </message>
    <message>
        <source>Song Name : </source>
        <translation>གླུ་དབྱངས་ཀྱི་མིང་ལ། </translation>
    </message>
    <message>
        <source>File Path : </source>
        <translation>ཡིག་ཆའི་འགྲོ་ལམ། </translation>
    </message>
    <message>
        <source>Album : </source>
        <translation>ཆེད་བསྒྲིགས། </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་ </translation>
    </message>
    <message>
        <source>Singer : </source>
        <translation>གཞས་པ། </translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ངོས་འཛིན་ </translation>
    </message>
    <message>
        <source>  Music Information</source>
        <translation>  གླུ་གཞས་ཆ་འཕྲིན་ </translation>
    </message>
    <message>
        <source>File Time : </source>
        <translation>གླུ་གཞས་ཀྱི་དུས་ཚོད་རིང་བ། </translation>
    </message>
    <message>
        <source>File Size : </source>
        <translation>ཡིག་ཆ་མང་ཉུང་། </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <source>Song</source>
        <translation>གླུ་གཞས་ </translation>
    </message>
    <message>
        <source>Time</source>
        <translation>དུས་ཡུན་རིང་བ་ </translation>
    </message>
    <message>
        <source>Album</source>
        <translation>ཆེད་སྒྲིག་ </translation>
    </message>
    <message>
        <source>Singer</source>
        <translation>གླུ་བ་ </translation>
    </message>
</context>
<context>
    <name>MyToolButton</name>
    <message>
        <source>Play</source>
        <translation>གཏོང་བ་</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation>གླུ་གཞས།</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation>ང་དགའ་པོ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <source>Loop</source>
        <translation>རེའུ་མིག་རྒྱུན་འཁོར། </translation>
    </message>
    <message>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་ཁྱབ་གཏོང་ </translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation></translation>
    </message>
    <message>
        <source>CurrentItemInLoop</source>
        <translation>རྐྱང་འཁྱོག་རྒྱུན་འཁོར། </translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <source>Loop</source>
        <translation>རེའུ་མིག་རྒྱུན་འཁོར། </translation>
    </message>
    <message>
        <source>Next</source>
        <translation>གཞས་རྗེས་མ་ </translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation>རོལ་དབྱངས། </translation>
    </message>
    <message>
        <source>00:00/00:00</source>
        <translation>00/00：00 </translation>
    </message>
    <message>
        <source>I Love</source>
        <translation>ང་དགའ་པོ་ཡོད། </translation>
    </message>
    <message>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་ཁྱབ་གཏོང་ </translation>
    </message>
    <message>
        <source>Volume</source>
        <translation>སྒྲ་ཚད་ </translation>
    </message>
    <message>
        <source>Play List</source>
        <translation>ལོ་རྒྱུས་སྟོན་པ། </translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>གཞས་སྔོན་མ་ </translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation>ཉར་ཚགས་ </translation>
    </message>
    <message>
        <source>CurrentItemInLoop</source>
        <translation>རྐྱང་འཁྱོག་རྒྱུན་འཁོར། </translation>
    </message>
    <message>
        <source>Play</source>
        <translation>རྩེད་མོ་རྩེ་བ།</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <source>Prompt information</source>
        <translation type="vanished">མགྱོགས་མྱུར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation type="vanished">ཡི་གེ་འདུས་མི་ཐུབ་པ། / * &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <source>Reached upper character limit</source>
        <translation type="vanished">སྟེང་གི་མི་སྣའི་ཚད་ལ་སླེབས་པ།</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">རོལ་དབྱངས།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>Please input playlist name:</source>
        <translation type="vanished">མིང་ཐོ་འགོད་རོགས།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Database Error</source>
        <translation>གཞི་གྲངས་མཛོད་ཀྱི་ནོར་འཁྲུལ་ </translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <source>Search Result</source>
        <translation>བཤེར་འཚོལ་མཇུག་འབྲས་ </translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <source>Album</source>
        <translation>ཆེད་སྒྲིག་ </translation>
    </message>
    <message>
        <source>Music</source>
        <translation>གླུ་གཞས་ </translation>
    </message>
    <message>
        <source>Singer</source>
        <translation>གླུ་བ་ </translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <source>Music Player</source>
        <translation>རོལ་དབྱངས། </translation>
    </message>
    <message>
        <source>My PlayList</source>
        <translation>ངའི་གཞས་ཐོ། </translation>
    </message>
    <message>
        <source>Library</source>
        <translation>རོལ་མོའི་མཛོད་ </translation>
    </message>
    <message>
        <source>Song List</source>
        <translation>གླུ་གཞས་རེའུ་མིག་ </translation>
    </message>
    <message>
        <source>I Love</source>
        <translation>ང་དགའ་པོ་ཡོད། </translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>བསྐྱར་དུ་མིང་བཏགས་པ་ </translation>
    </message>
    <message>
        <source>New Playlist</source>
        <translation>གསར་དུ་བསྐྲུན་པའི་གཞས་ཐོ་ </translation>
    </message>
    <message>
        <source>Single song name already exists!!!</source>
        <translation>གཞས་ཀྱི་ཁེར་མིང་ཡོད། </translation>
    </message>
    <message>
        <source>Prompt information</source>
        <translation>གསལ་འདེབས་ཆ་འཕྲིན། </translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>Play</source>
        <translation>གཏོང་བ་ </translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>གཙང་སེལ་ </translation>
    </message>
    <message>
        <source> songs</source>
        <translation>མགོ་ </translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">ལོ་རྒྱུས་སྟོན་པ། </translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>སུབ་པ་ </translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">མཉམ་དུ། </translation>
    </message>
    <message>
        <source>The playlist has no songs</source>
        <translation>ཁྱབ་གཏོང་རེའུ་མིག་ད་དུང་གླུ་གཞས་མི་འདུག </translation>
    </message>
    <message>
        <source>Clear the playlist?</source>
        <translation>ཆིང་ཁུང་གིས་བསྟན་པའི་རེའུ་མིག་རེད་དམ། </translation>
    </message>
    <message>
        <source>Play the next one</source>
        <translation>གཞས་རྗེས་མ་གཏོང་། </translation>
    </message>
    <message>
        <source>Prompt information</source>
        <translation>གསལ་འདེབས་ཆ་འཕྲིན། </translation>
    </message>
    <message>
        <source>History</source>
        <translation>ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="obsolete"> གླུ་དབྱངས།</translation>
    </message>
    <message>
        <source>song</source>
        <translation type="vanished">སམོུ</translation>
    </message>
    <message>
        <source>songs</source>
        <translation type="vanished">སམོུ</translation>
    </message>
    <message>
        <source>total</source>
        <translation type="vanished">ངོནང</translation>
    </message>
    <message>
        <source>path does not exist</source>
        <translation>འགྲོ་ལམ་མེད་པ།</translation>
    </message>
    <message>
        <source>kylin-music</source>
        <translation>ཡིནཡེུ</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">སྣོན་པ་ </translation>
    </message>
    <message>
        <source>Play</source>
        <translation>གཏོང་བ་ </translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">གླུ་གཞས་ </translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">དུས་ཡུན་རིང་བ་ </translation>
    </message>
    <message>
        <source>Audio File</source>
        <translation>སྒྲ་ཟློས་ཡིག་ཆ། </translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">ཆེད་སྒྲིག་ </translation>
    </message>
    <message>
        <source>Add local folders</source>
        <translation>ས་གནས་དེ་གའི་ཡིག་ཁུག་ཁ་སྣོན། </translation>
    </message>
    <message>
        <source>Add Local Folder</source>
        <translation>ས་གནས་དེ་གའི་ཡིག་ཁུག་ཁ་སྣོན། </translation>
    </message>
    <message>
        <source> songs</source>
        <translation>མགོ་ </translation>
    </message>
    <message>
        <source>Play All</source>
        <translation>ཚང་མ་ཁྱབ་གཏོང </translation>
    </message>
    <message>
        <source>Remove from local</source>
        <translation>རང་ས་ནས་སུབ་པ། </translation>
    </message>
    <message>
        <source>Add to songlist</source>
        <translation>གཞས་ཐོ་སྣོན་པ། </translation>
    </message>
    <message>
        <source>Song List</source>
        <translation>གླུ་གཞས་རེའུ་མིག་ </translation>
    </message>
    <message>
        <source>I Love</source>
        <translation>ང་དགའ་པོ་ཡོད། </translation>
    </message>
    <message>
        <source>path does not exist</source>
        <translation>འགྲོ་ལམ་མི་འདུག </translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">གླུ་བ་ </translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">མཉམ་དུ། </translation>
    </message>
    <message>
        <source>There are no songs!</source>
        <translation>ད་རུང་གླུ་དབྱངས་མེད། </translation>
    </message>
    <message>
        <source>Add failed</source>
        <translation>སྦྱོར་རྟ་ཕམ་པ། </translation>
    </message>
    <message>
        <source>Add Local Songs</source>
        <translation>ས་གནས་དེ་གའི་གླུ་དབྱངས་ཁ་སྣོན་བྱེད། </translation>
    </message>
    <message>
        <source>Add local songs</source>
        <translation>ས་གནས་དེ་གའི་གླུ་དབྱངས་ཁ་སྣོན་བྱེད། </translation>
    </message>
    <message>
        <source>Delete from list</source>
        <translation>གླུ་རྐྱང་ཁྲོད་ནས་སུབ་པ་། </translation>
    </message>
    <message>
        <source>Repeat add</source>
        <translation>བསྐྱར་སྣོན་ </translation>
    </message>
    <message>
        <source>Prompt information</source>
        <translation>གསལ་འདེབས་ཆ་འཕྲིན། </translation>
    </message>
    <message>
        <source>Search Result</source>
        <translation>བཤེར་འཚོལ་མཇུག་འབྲས་ </translation>
    </message>
    <message>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>གཞས་ས་ཆ་དེ་རང་ནས་སུབ་རྗེས་སླར་གསོ་མི་ཐུབ་པས།སུབ་རྒྱུ་གཏན་འཁེལ་བྱེད་དམ། </translation>
    </message>
    <message>
        <source>Success add %1 songs</source>
        <translation>གཞས་1%གྲུབ་འབྲས་ཐོབ་པའི་ངང་ཁ་སྣོན་བརྒྱབ་པ། </translation>
    </message>
    <message>
        <source>View song information</source>
        <translation>གླུ་གཞས་ཆ་འཕྲིན་ལ་ལྟ་ཞིབ་། </translation>
    </message>
    <message>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>འདེམས་ཐོན་བྱུང་བའི་གཞས་དེ་གཞས་རྐྱང་ནང་ནས་སུབ་ཀྱི་རེད་དམ། </translation>
    </message>
    <message>
        <source>Add failed! Format error or no access permission!</source>
        <translation type="vanished">རྣམ་གཞག་གི་ཡིག་ཆ་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>Add Music</source>
        <translation>རོལ་དབྱངས་ཁ་སྣོན་བྱས</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation type="vanished">ཁ་ཕྱེ་བའི་ཡིག་སྣོད</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> གླུ་དབྱངས།</translation>
    </message>
    <message>
        <source>Success add %1 song</source>
        <translation type="vanished">གླུ་དབྱངས་%lཁ་སྣོན་བྱས་བ་ལེགས་གྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <source>Clear List</source>
        <translation type="vanished">གསལ་པོར་བཤད་ན།</translation>
    </message>
    <message>
        <source>Are you sure to clear the list?</source>
        <translation type="vanished">ཁྱོད་ཀྱིས་ངེས་པར་དུ་མིང་ཐོ་གཙང་བཤེར་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Delete Selected Items</source>
        <translation type="vanished">བདམས་ཟིན་པའི་རྣམ་གྲངས་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Song Information</source>
        <translation type="vanished">གླུ་དབྱངས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Open The Folder</source>
        <translation type="vanished">ཡིག་སྣོད་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Add To Songlist</source>
        <translation type="vanished">གླུ་དབྱངས་ཀྱི་མིང་ཐོའི་ནང་ཞུགས་པ།</translation>
    </message>
    <message>
        <source>kylin-music</source>
        <translation>ཡིནཡེུ</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>No</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>This format file is not supported</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་ཡིག་ཆ་དེར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>back</source>
        <translation>ཕྱིར་བཤོལ་ </translation>
    </message>
    <message>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ་ </translation>
    </message>
    <message>
        <source>To minimize the</source>
        <translation>ཆེས་ཆུང་ཅན་ </translation>
    </message>
    <message>
        <source>Not logged in</source>
        <translation>ཐོ་འགོད་མ་བྱས་པ་ </translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>ཆེ་ཤོས་སུ་སྒྱུར་བ་ </translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">རོལ་དབྱངས་བཤེར་འཚོལ་དང་།གླུ་བ་། </translation>
    </message>
    <message>
        <source>mini model</source>
        <translation>ཁུ་བསྡུ་བྱེད་སྟངས་ </translation>
    </message>
    <message>
        <source>forward</source>
        <translation>མདུན་དུ་སྐྱོད་པ་ </translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>Music Player</source>
        <translation>རོལ་དབྱངས། </translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>ཆེ་ཤོས་སུ་སྒྱུར་བ་ </translation>
    </message>
    <message>
        <source>reduction</source>
        <translation>སོར་ལོག་ </translation>
    </message>
    <message>
        <source>Prompt information</source>
        <translation>གསལ་འདེབས་ཆ་འཕྲིན། </translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Auto</source>
        <translation>རང་འགུལ་ </translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>གནག་པོ། </translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ། </translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">ཕྱིར་འབུད་ </translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation>རོལ་དབྱངས། </translation>
    </message>
    <message>
        <source>About</source>
        <translation>སྐོར་ </translation>
    </message>
    <message>
        <source>Light</source>
        <translation>མདོག་སྐྱ་བོ་ </translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ། </translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">གླུ་དབྱངས་གཏོང་ཆས་ནི་གླུ་དབྱངས་ཡིག་ཆ་སྣ་ཚོགས་གཏོང་བྱེད་ཀྱི་སྨྱན་མང་གཏོང་བྱེད་ཀྱི་མཉེན་ཆས་ཤིག་རེད། དེ་ནི་རོལ་དབྱངས་ཀྱི་རྣམ་པ་སྣ་ཚོགས་ཚུད་ཡོད་།གཏོང་བའི་ལག་ཆ་དང་།བཀོལ་སྤྱོད་མགྱོགས་ལ་སྟབས་བདེ་བ་། </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ། </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>པར་གཞི། </translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation type="vanished">རོལ་མོ་དཀྲོལ་མཁན་ནི་སྨྱན་མང་གཟུགས་ཀྱི་ཕྱིར་ལྡོག་མཉེན་ཆས་ཤིག་ཡིན། རོལ་མོའི་རྣམ་གཞག་སྣ་ཚོགས་ཀྱི་རོལ་རྩེད་ཕྱིར་ལྡོག་ཡོ་བྱད་ལ་ཁེབས་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>ཟས་ཐོ།</translation>
    </message>
    <message>
        <source>kylin-music</source>
        <translation type="vanished">ཡིནཡེུ</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>ཏིུཀམུ</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">བསལ་འདེམས་ཀྱི་དབང་ཆ།</translation>
    </message>
</context>
<context>
    <name>miniWidget</name>
    <message>
        <source>Loop</source>
        <translation>རེའུ་མིག་རྒྱུན་འཁོར།</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་རང་བཞིན།</translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation>གོ་རིམ།</translation>
    </message>
    <message>
        <source>CurrentItemInLoop</source>
        <translation>རྐྱང་འཁྱོག་རྒྱུན་འཁོར།</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>རྩེད་མོ་རྩེ་བ།</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation>རོལ་དབྱངས།</translation>
    </message>
    <message>
        <source>00:00/00:00</source>
        <translation></translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>གཞས་སྔོན་མ་ </translation>
    </message>
    <message>
        <source>Next</source>
        <translation>གཞས་རྗེས་མ་ </translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation>ཉར་ཚགས་ </translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>normal mode</source>
        <translation>རྒྱུན་ལྡན་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation>ང་དགའ་པོ་ཡོད།</translation>
    </message>
    <message>
        <source>kylin-music mini</source>
        <translation type="unfinished">ཡིནཡེུ</translation>
    </message>
</context>
</TS>
