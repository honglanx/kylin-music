<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>AllPupWindow</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <location filename="../UI/base/allpupwindow.cpp" line="87"/>
        <source>Prompt information</source>
        <translation>ەسكەرتمە ۇچۇرلارى</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Font limit exceeded</source>
        <translation type="vanished">超出字数限制</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>ۅز ىچىنە  ئالمايدىغان تۅمۅندۅكۉ  تامعالار : / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="53"/>
        <source>Reached upper character limit</source>
        <translation>تامعا سانى  جوعورۇ چەككە جەتى</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="62"/>
        <source>Music Player</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="115"/>
        <source>Confirm</source>
        <translation>بەكىتۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="119"/>
        <source>Cancel</source>
        <translation>ارعادان  قالتىرىش</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="128"/>
        <source>Please input playlist name:</source>
        <translation>قويۇش  تىزىمدىگىنىن  اتاعىن كىرگىزىڭ:</translation>
    </message>
    <message>
        <source>Please enter the title of the playlist:</source>
        <translation type="vanished">请输入歌单标题：</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="229"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="265"/>
        <source>Unknown singer</source>
        <translation>بەلگىسىز  ناخشىچى</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="230"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="268"/>
        <source>Unknown album</source>
        <translation>بەلگىسىز  پلاستىنكا</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Prompt information</source>
        <translation>ەسكەرتمە ۇچۇرلارى</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Add failed, no valid music file found</source>
        <translation>قوشۇۇ  جەڭىلۉۉ بولۇش ،ۅنۉمدۉ مۇزىكا  ۅجۅتۉۉ تابىلبادى</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="270"/>
        <source>  Music Information</source>
        <translation>  مۇزىكا  ۇچۇرۇ</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="231"/>
        <source>Music Player</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="297"/>
        <source>Confirm</source>
        <translation>بەكىتۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="300"/>
        <source>Cancel</source>
        <translation>ارعادان  قالتىرىش</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="247"/>
        <location filename="../UI/base/allpupwindow.cpp" line="323"/>
        <source>Song Name : </source>
        <translation>ىر اتى  : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="248"/>
        <location filename="../UI/base/allpupwindow.cpp" line="324"/>
        <source>Singer : </source>
        <translation>ناخشىچى : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="249"/>
        <location filename="../UI/base/allpupwindow.cpp" line="325"/>
        <source>Album : </source>
        <translation>ئالبۇم : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="250"/>
        <location filename="../UI/base/allpupwindow.cpp" line="326"/>
        <source>File Type : </source>
        <translation>ۅجۅت تۉرۉ : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="251"/>
        <location filename="../UI/base/allpupwindow.cpp" line="327"/>
        <source>File Size : </source>
        <translation>ۅجۅت چوڭدۇعۇ  : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="252"/>
        <location filename="../UI/base/allpupwindow.cpp" line="328"/>
        <source>File Time : </source>
        <translation>ۅجۅت ۇباقتى : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="253"/>
        <location filename="../UI/base/allpupwindow.cpp" line="329"/>
        <source>File Path : </source>
        <translation>ۅجۅت جولۇ : </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="7"/>
        <source>Song</source>
        <translation>ىر</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Singer</source>
        <translation>ناخشىچى</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Album</source>
        <translation>ئالبۇم</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Time</source>
        <translation>ۇباقىتتا</translation>
    </message>
</context>
<context>
    <name>MyToolButton</name>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="142"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="155"/>
        <source>Play</source>
        <translation>قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="143"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="159"/>
        <source>Pause</source>
        <translation>توقتوتۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="144"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="163"/>
        <source>Rename</source>
        <translation>قايرا ات تەك ات  جاسوو ،اتقارۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="145"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="167"/>
        <source>Delete</source>
        <translation>ۅچۉرۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>Song List</source>
        <translation>ىر تىزىمدىگى</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>I Love</source>
        <translation>مەن جاقشى كۆرىمەن</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>دووردۇق ايلانما قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>ۅز ەركىنچە  قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>تارتىپ  بويۇنچا  قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>تاق مۇزىكا  ايلانما قويۇش</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="32"/>
        <source>Previous</source>
        <translation>الدىنقى بەت</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="41"/>
        <source>Next</source>
        <translation>كىيىنكى  بەت</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="45"/>
        <source>Volume</source>
        <translation>دووش</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="106"/>
        <source>Favourite</source>
        <translation>جاقشى كۅرۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="133"/>
        <location filename="../UI/player/playsongarea.cpp" line="149"/>
        <location filename="../UI/player/playsongarea.cpp" line="473"/>
        <location filename="../UI/player/playsongarea.cpp" line="519"/>
        <source>Loop</source>
        <translation>دووردۇق ايلانما قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="168"/>
        <source>Play List</source>
        <translation>قويۇش  بايىرقى</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="188"/>
        <location filename="../UI/player/playsongarea.cpp" line="545"/>
        <location filename="../UI/player/playsongarea.cpp" line="547"/>
        <location filename="../UI/player/playsongarea.cpp" line="642"/>
        <location filename="../UI/player/playsongarea.cpp" line="875"/>
        <location filename="../UI/player/playsongarea.cpp" line="903"/>
        <source>Music Player</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="189"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="416"/>
        <location filename="../UI/player/playsongarea.cpp" line="433"/>
        <source>I Love</source>
        <translation>مەن جاقشى كۆرىمەن</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="141"/>
        <location filename="../UI/player/playsongarea.cpp" line="151"/>
        <location filename="../UI/player/playsongarea.cpp" line="482"/>
        <location filename="../UI/player/playsongarea.cpp" line="524"/>
        <source>Random</source>
        <translation>ۅز ەركىنچە  قويۇش</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="125"/>
        <location filename="../UI/player/playsongarea.cpp" line="153"/>
        <location filename="../UI/player/playsongarea.cpp" line="499"/>
        <location filename="../UI/player/playsongarea.cpp" line="509"/>
        <source>CurrentItemInLoop</source>
        <translation>تاق مۇزىكا  ايلانما قويۇش</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="64"/>
        <source>Database Error</source>
        <translation>سالقىنساما كەپىچ   قاتالىعى</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="120"/>
        <source>Search Result</source>
        <translation>ىزدۅۅ ناتىيجاسى</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="33"/>
        <source>Music</source>
        <translation>ىر-مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="49"/>
        <source>Singer</source>
        <translation>ناخشىچى</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="65"/>
        <source>Album</source>
        <translation>ئالبۇم</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="55"/>
        <source>Music Player</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="73"/>
        <source>Library</source>
        <translation>مۇزىكا  قازىناعى</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="82"/>
        <source>Song List</source>
        <translation>ىر تىزىمدىگى</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="94"/>
        <source>My PlayList</source>
        <translation>قويۇش  تىزىمدىگىم</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="139"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="345"/>
        <source>New Playlist</source>
        <translation>جاڭى قويۇلۇش  تىزىمدىگى</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="143"/>
        <source>Rename</source>
        <translation>قايرا ات تەك ات  جاسوو ،اتقارۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="253"/>
        <source>I Love</source>
        <translation>مەن جاقشى كۆرىمەن</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="304"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="392"/>
        <source>Prompt information</source>
        <translation>ەسكەرتمە ۇچۇرلارى</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="304"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="392"/>
        <source>Single song name already exists!!!</source>
        <translation>ىر تىزىمدىگى الدا مۇرۇن  باربولۇۇسۇ !!</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="41"/>
        <source>History</source>
        <translation>قويۇش  بايىرقى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="49"/>
        <source>Empty</source>
        <translation>كۅڭدۅي</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="67"/>
        <source>The playlist has no songs</source>
        <translation>مۇزىكا  جوق</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="121"/>
        <source>Prompt information</source>
        <translation>ەسكەرتمە ۇچۇرلارى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="121"/>
        <source>Clear the playlist?</source>
        <translation>قويۇلۇش  تىزىمدىگىن  قۇرۇقدامسىز؟</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="122"/>
        <source>kylin-music</source>
        <translation>kylin-مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="359"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="390"/>
        <source>path does not exist</source>
        <translation>جول باربولۇۇسۇ  ەمەس</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="249"/>
        <source> songs</source>
        <translation> الدىنداقى  ىر</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="278"/>
        <source>Play</source>
        <translation>قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="279"/>
        <source>Delete</source>
        <translation>ۅچۉرۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="280"/>
        <source>Play the next one</source>
        <translation>كىيىنكىسىن قويۇش</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="259"/>
        <location filename="../UI/tableview/tableone.cpp" line="1093"/>
        <location filename="../UI/tableview/tableone.cpp" line="1134"/>
        <location filename="../UI/tableview/tableone.cpp" line="1206"/>
        <source>Song List</source>
        <translation>ىر تىزىمدىگى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="191"/>
        <source>There are no songs!</source>
        <translation>مۇزىكا  جوق</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="147"/>
        <source>Play All</source>
        <translation>باردىعىن قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="150"/>
        <source>Add Music</source>
        <translation>مۇزىكا  قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="156"/>
        <source>Add local songs</source>
        <translation>جابدۇۇسۇنۇن ۅزۉندۅكۉ مۇزىكانى قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <source>Add local folders</source>
        <translation>جابدۇۇسۇنۇن ۅزۉندۅكۉ  ۅجۅتۉن قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="193"/>
        <source>Add Local Songs</source>
        <translation>جابدۇۇسۇنۇن ۅزۉندۅكۉ مۇزىكانى قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="194"/>
        <source>Add Local Folder</source>
        <translation>جابدۇۇسۇنۇن ۅزۉندۅكۉ  ۅجۅتتۅرۉن قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="261"/>
        <location filename="../UI/tableview/tableone.cpp" line="344"/>
        <location filename="../UI/tableview/tableone.cpp" line="699"/>
        <location filename="../UI/tableview/tableone.cpp" line="1095"/>
        <location filename="../UI/tableview/tableone.cpp" line="1136"/>
        <source>I Love</source>
        <translation>مەن جاقشى كۆرىمەن</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="263"/>
        <location filename="../UI/tableview/tableone.cpp" line="1097"/>
        <location filename="../UI/tableview/tableone.cpp" line="1138"/>
        <location filename="../UI/tableview/tableone.cpp" line="1183"/>
        <location filename="../UI/tableview/tableone.cpp" line="1232"/>
        <location filename="../UI/tableview/tableone.cpp" line="1261"/>
        <source>Search Result</source>
        <translation>ىزدۅۅ ناتىيجاسى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="291"/>
        <source>Play</source>
        <translation>قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="292"/>
        <source>Delete from list</source>
        <translation>تىزىمدىكىتىن ۅچۉرۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="293"/>
        <source>Remove from local</source>
        <translation>يەرلىكتىن چىعارۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="368"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>تاندالعان  ىردى ىر  تىزىمدىكدەن ئۆچۈرۈلىدىغانلىقىنى بەكىتەسىزبى؟</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="369"/>
        <location filename="../UI/tableview/tableone.cpp" line="522"/>
        <location filename="../UI/tableview/tableone.cpp" line="623"/>
        <location filename="../UI/tableview/tableone.cpp" line="729"/>
        <location filename="../UI/tableview/tableone.cpp" line="805"/>
        <location filename="../UI/tableview/tableone.cpp" line="931"/>
        <location filename="../UI/tableview/tableone.cpp" line="942"/>
        <location filename="../UI/tableview/tableone.cpp" line="953"/>
        <location filename="../UI/tableview/tableone.cpp" line="960"/>
        <source>kylin-music</source>
        <translation>kylin-مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="370"/>
        <location filename="../UI/tableview/tableone.cpp" line="396"/>
        <location filename="../UI/tableview/tableone.cpp" line="523"/>
        <location filename="../UI/tableview/tableone.cpp" line="624"/>
        <location filename="../UI/tableview/tableone.cpp" line="943"/>
        <location filename="../UI/tableview/tableone.cpp" line="961"/>
        <source>Yes</source>
        <translation>وشوندوي</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="371"/>
        <location filename="../UI/tableview/tableone.cpp" line="397"/>
        <location filename="../UI/tableview/tableone.cpp" line="524"/>
        <location filename="../UI/tableview/tableone.cpp" line="625"/>
        <source>No</source>
        <translation>جوق</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="395"/>
        <location filename="../UI/tableview/tableone.cpp" line="521"/>
        <location filename="../UI/tableview/tableone.cpp" line="622"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>ىر ۅچۉرۉلگۅندۅن كىيىن العاچىنا گەلتىرىشكە  بولبويت . ىراس ەلە ۅچۉرۅسۉزبۉ؟</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="775"/>
        <source>Audio File</source>
        <translation>دوبۇش -ايىپ  ۅجۅتۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="921"/>
        <source>This format file is not supported</source>
        <translation>بۇل فورمات  ۅجۅتۉن  قولدوبويت</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="959"/>
        <source>Repeat add</source>
        <translation>قوشۇشتۇ  قايتالوو</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="995"/>
        <source>path does not exist</source>
        <translation>جول باربولۇۇسۇ  ەمەس</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="930"/>
        <location filename="../UI/tableview/tableone.cpp" line="952"/>
        <source>Success add %1 songs</source>
        <translation>1%ىردى جەڭىشتۉۉ قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="941"/>
        <source>Add failed</source>
        <translation>قوشۇۇ  جەڭىلۉۉ بولدۇ</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="294"/>
        <source>View song information</source>
        <translation>ىر ۇچۇرلارى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="295"/>
        <source>Add to songlist</source>
        <translation>ىر تىزىمدىگىنە  قوش</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="368"/>
        <location filename="../UI/tableview/tableone.cpp" line="395"/>
        <location filename="../UI/tableview/tableone.cpp" line="521"/>
        <location filename="../UI/tableview/tableone.cpp" line="622"/>
        <location filename="../UI/tableview/tableone.cpp" line="930"/>
        <location filename="../UI/tableview/tableone.cpp" line="941"/>
        <location filename="../UI/tableview/tableone.cpp" line="952"/>
        <location filename="../UI/tableview/tableone.cpp" line="959"/>
        <source>Prompt information</source>
        <translation>ەسكەرتمە ۇچۇرلارى</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1031"/>
        <source> songs</source>
        <translation> الدىنداقى  ىر</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="57"/>
        <source>back</source>
        <translation>جانىش ، قايتىش</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="69"/>
        <source>forward</source>
        <translation>ىلگەرلۅۅ</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="92"/>
        <source>Not logged in</source>
        <translation>تىزىمدەتىلبەگەن</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="155"/>
        <source>mini model</source>
        <translation>جۅنۅكۅيلۅشتۉرۉلگۅن كەبەتەسى ، تۇرپاتى</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="164"/>
        <source>To minimize the</source>
        <translation>كىچىرەيتۉۉ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="175"/>
        <source>maximize</source>
        <translation>چوڭويتۇش</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="186"/>
        <source>close</source>
        <translation>جابۇۇ</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="599"/>
        <source>Music Player</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="848"/>
        <location filename="../UI/mainwidget.cpp" line="1047"/>
        <source>reduction</source>
        <translation>العاچىنا كەتىرىش</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="854"/>
        <location filename="../UI/mainwidget.cpp" line="1039"/>
        <source>maximize</source>
        <translation>چوڭويتۇش</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1173"/>
        <source>Prompt information</source>
        <translation>ەسكەرتمە ۇچۇرلارى</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="32"/>
        <source>Menu</source>
        <translation>تىزىمدىك</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="44"/>
        <source>Theme</source>
        <translation>ۇسلۇپ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="46"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="150"/>
        <source>Help</source>
        <translation>جەرلىك</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="48"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="148"/>
        <source>About</source>
        <translation>جۅنۉندۅ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="50"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="146"/>
        <source>Exit</source>
        <translation>سىندىرۇۇ  ووزۇ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="57"/>
        <source>Auto</source>
        <translation>اپتوماتتىك</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="62"/>
        <source>Light</source>
        <translation>جەڭىل</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="67"/>
        <source>Dark</source>
        <translation>قانىق  تۉس</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="254"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="295"/>
        <source>Music Player</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="289"/>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation>مۇزىكا   قويعۇچ بولسو ار بىر تۉر  كەبەتەسىندەكى  مۇزىكىلارنى كۅپ ارعا قويۇش   دەتالدا ىشتەتىلەت. ال  ار بىر تۉر  مۇزىكا   تۉرۉندۅگۉ قويۇش   قوراللىرىنى ۅز ىچىنە  الات، ماشعۇلاتى تەز جانا جۆنۆكۆي بولوت .</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="299"/>
        <source>Version: </source>
        <translation>باسماسى : </translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="356"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="364"/>
        <source>Service &amp; Support: </source>
        <translation>تەيلۅۅ &gt; قولدوش ، دەم بەرىش : </translation>
    </message>
</context>
<context>
    <name>miniWidget</name>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="43"/>
        <location filename="../UI/player/miniwidget.cpp" line="268"/>
        <location filename="../UI/player/miniwidget.cpp" line="614"/>
        <location filename="../UI/player/miniwidget.cpp" line="719"/>
        <source>Music Player</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="60"/>
        <location filename="../UI/player/miniwidget.cpp" line="61"/>
        <source>列表循环</source>
        <translation>تىزىمدىك  بويۇنچا  قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="62"/>
        <source>随机播放</source>
        <translation>ۅزەركى مەنەن قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="63"/>
        <source>顺序播放</source>
        <translation>تارتىپ  بويۇنچا  قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="64"/>
        <source>单曲循环</source>
        <translation>تاق مۇزىكا  تارتىبىندا قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="269"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="330"/>
        <location filename="../UI/player/miniwidget.cpp" line="697"/>
        <source>CurrentItemInLoop</source>
        <translation>تاق مۇزىكا  ايلانما قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="509"/>
        <location filename="../UI/player/miniwidget.cpp" line="525"/>
        <source>I Love</source>
        <translation>مەن جاقشى كۆرىمەن</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="338"/>
        <location filename="../UI/player/miniwidget.cpp" line="702"/>
        <source>Loop</source>
        <translation>دووردۇق ايلانما قويۇش</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="346"/>
        <location filename="../UI/player/miniwidget.cpp" line="707"/>
        <source>Random</source>
        <translation>ۅز ەركىنچە  قويۇش</translation>
    </message>
</context>
</TS>
