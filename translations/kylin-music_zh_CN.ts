<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AllPupWindow</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <location filename="../UI/base/allpupwindow.cpp" line="87"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Font limit exceeded</source>
        <translation type="vanished">超出字数限制</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>不能包含以下字符：\ / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="53"/>
        <source>Reached upper character limit</source>
        <translation>已达到字符上限</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="62"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="115"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="119"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="128"/>
        <source>Please input playlist name:</source>
        <translation>请输入歌单名称：</translation>
    </message>
    <message>
        <source>Please enter the title of the playlist:</source>
        <translation type="vanished">请输入歌单标题：</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="229"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="265"/>
        <source>Unknown singer</source>
        <translation>未知歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="230"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="268"/>
        <source>Unknown album</source>
        <translation>未知专辑</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Add failed, no valid music file found</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="299"/>
        <source>  Music Information</source>
        <translation>  歌曲信息</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="240"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="387"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="389"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="312"/>
        <source>Song Name : </source>
        <translation>歌曲名：</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="313"/>
        <source>Singer : </source>
        <translation>歌手：</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="314"/>
        <source>Album : </source>
        <translation>专辑：</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="315"/>
        <source>File Type : </source>
        <translation>文件类型：</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="316"/>
        <source>File Size : </source>
        <translation>文件大小：</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="317"/>
        <source>File Time : </source>
        <translation>歌曲时长：</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="318"/>
        <source>File Path : </source>
        <translation>文件路径：</translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="7"/>
        <source>Song</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Time</source>
        <translation>时长</translation>
    </message>
</context>
<context>
    <name>MyToolButton</name>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="142"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="155"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="143"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="159"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="144"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="163"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="145"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="167"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>列表循环</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>随机播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>单曲循环</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="32"/>
        <source>Previous</source>
        <translation>上一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="41"/>
        <source>Next</source>
        <translation>下一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="45"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="106"/>
        <source>Favourite</source>
        <translation>收藏</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="133"/>
        <location filename="../UI/player/playsongarea.cpp" line="149"/>
        <location filename="../UI/player/playsongarea.cpp" line="486"/>
        <location filename="../UI/player/playsongarea.cpp" line="532"/>
        <source>Loop</source>
        <translation>列表循环</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="168"/>
        <source>Play List</source>
        <translation>播放历史</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="188"/>
        <location filename="../UI/player/playsongarea.cpp" line="558"/>
        <location filename="../UI/player/playsongarea.cpp" line="560"/>
        <location filename="../UI/player/playsongarea.cpp" line="673"/>
        <location filename="../UI/player/playsongarea.cpp" line="906"/>
        <location filename="../UI/player/playsongarea.cpp" line="934"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="189"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="429"/>
        <location filename="../UI/player/playsongarea.cpp" line="446"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="607"/>
        <location filename="../UI/player/playsongarea.cpp" line="977"/>
        <location filename="../UI/player/playsongarea.cpp" line="1033"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="617"/>
        <location filename="../UI/player/playsongarea.cpp" line="989"/>
        <location filename="../UI/player/playsongarea.cpp" line="1001"/>
        <location filename="../UI/player/playsongarea.cpp" line="1045"/>
        <location filename="../UI/player/playsongarea.cpp" line="1057"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="141"/>
        <location filename="../UI/player/playsongarea.cpp" line="151"/>
        <location filename="../UI/player/playsongarea.cpp" line="495"/>
        <location filename="../UI/player/playsongarea.cpp" line="537"/>
        <source>Random</source>
        <translation>随机播放</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="125"/>
        <location filename="../UI/player/playsongarea.cpp" line="153"/>
        <location filename="../UI/player/playsongarea.cpp" line="512"/>
        <location filename="../UI/player/playsongarea.cpp" line="522"/>
        <source>CurrentItemInLoop</source>
        <translation>单曲循环</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="64"/>
        <source>Database Error</source>
        <translation>数据库错误</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="120"/>
        <source>Search Result</source>
        <translation>搜索结果</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="34"/>
        <source>Music</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="50"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="66"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="58"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="88"/>
        <source>Library</source>
        <translation>音乐库</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="97"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="109"/>
        <source>My PlayList</source>
        <translation>我的歌单</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="154"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="363"/>
        <source>New Playlist</source>
        <translation>新建歌单</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="158"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="268"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Single song name already exists!!!</source>
        <translation>歌单名已存在！</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="44"/>
        <source>History</source>
        <translation>播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="52"/>
        <source>Empty</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="70"/>
        <source>The playlist has no songs</source>
        <translation>无音乐</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <source>Clear the playlist?</source>
        <translation>清空播放列表？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="131"/>
        <source>kylin-music</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="383"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="411"/>
        <source>path does not exist</source>
        <translation>路径不存在</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="265"/>
        <source> songs</source>
        <translation> 首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="294"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="295"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="296"/>
        <source>Play the next one</source>
        <translation>下一首播放</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="258"/>
        <location filename="../UI/tableview/tableone.cpp" line="1127"/>
        <location filename="../UI/tableview/tableone.cpp" line="1168"/>
        <location filename="../UI/tableview/tableone.cpp" line="1240"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="191"/>
        <source>There are no songs!</source>
        <translation>无音乐</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="147"/>
        <source>Play All</source>
        <translation>播放全部</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="150"/>
        <source>Add Music</source>
        <translation>添加音乐</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="156"/>
        <source>Add local songs</source>
        <translation>添加本地音乐</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <source>Add local folders</source>
        <translation>添加本地文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="192"/>
        <source>Add Local Songs</source>
        <translation>添加本地音乐</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="193"/>
        <source>Add Local Folder</source>
        <translation>添加本地文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="260"/>
        <location filename="../UI/tableview/tableone.cpp" line="343"/>
        <location filename="../UI/tableview/tableone.cpp" line="716"/>
        <location filename="../UI/tableview/tableone.cpp" line="1129"/>
        <location filename="../UI/tableview/tableone.cpp" line="1170"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="262"/>
        <location filename="../UI/tableview/tableone.cpp" line="1131"/>
        <location filename="../UI/tableview/tableone.cpp" line="1172"/>
        <location filename="../UI/tableview/tableone.cpp" line="1217"/>
        <location filename="../UI/tableview/tableone.cpp" line="1266"/>
        <location filename="../UI/tableview/tableone.cpp" line="1295"/>
        <source>Search Result</source>
        <translation>搜索结果</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="290"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="291"/>
        <source>Delete from list</source>
        <translation>从歌单中删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="292"/>
        <source>Remove from local</source>
        <translation>从本地删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="367"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>确认将选中的歌曲从歌单中删除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="369"/>
        <location filename="../UI/tableview/tableone.cpp" line="529"/>
        <location filename="../UI/tableview/tableone.cpp" line="633"/>
        <location filename="../UI/tableview/tableone.cpp" line="746"/>
        <location filename="../UI/tableview/tableone.cpp" line="826"/>
        <location filename="../UI/tableview/tableone.cpp" line="955"/>
        <location filename="../UI/tableview/tableone.cpp" line="966"/>
        <location filename="../UI/tableview/tableone.cpp" line="980"/>
        <location filename="../UI/tableview/tableone.cpp" line="990"/>
        <source>kylin-music</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="372"/>
        <location filename="../UI/tableview/tableone.cpp" line="401"/>
        <location filename="../UI/tableview/tableone.cpp" line="530"/>
        <location filename="../UI/tableview/tableone.cpp" line="634"/>
        <location filename="../UI/tableview/tableone.cpp" line="967"/>
        <location filename="../UI/tableview/tableone.cpp" line="991"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="373"/>
        <location filename="../UI/tableview/tableone.cpp" line="402"/>
        <location filename="../UI/tableview/tableone.cpp" line="531"/>
        <location filename="../UI/tableview/tableone.cpp" line="635"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="397"/>
        <location filename="../UI/tableview/tableone.cpp" line="527"/>
        <location filename="../UI/tableview/tableone.cpp" line="631"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>歌曲从本地删除后不可恢复，是否确定删除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="796"/>
        <source>Audio File</source>
        <translation>音频文件</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="945"/>
        <source>This format file is not supported</source>
        <translation>不支持此格式文件</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="989"/>
        <source>Repeat add</source>
        <translation>重复添加</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1028"/>
        <source>path does not exist</source>
        <translation>路径不存在</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="954"/>
        <location filename="../UI/tableview/tableone.cpp" line="979"/>
        <source>Success add %1 songs</source>
        <translation>成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="965"/>
        <source>Add failed</source>
        <translation>添加失败</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="293"/>
        <source>View song information</source>
        <translation>查看歌曲信息</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="294"/>
        <source>Add to songlist</source>
        <translation>添加到歌单</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="367"/>
        <location filename="../UI/tableview/tableone.cpp" line="397"/>
        <location filename="../UI/tableview/tableone.cpp" line="527"/>
        <location filename="../UI/tableview/tableone.cpp" line="631"/>
        <location filename="../UI/tableview/tableone.cpp" line="954"/>
        <location filename="../UI/tableview/tableone.cpp" line="965"/>
        <location filename="../UI/tableview/tableone.cpp" line="979"/>
        <location filename="../UI/tableview/tableone.cpp" line="989"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1065"/>
        <source> songs</source>
        <translation> 首歌曲</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="58"/>
        <source>back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="70"/>
        <source>forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="79"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="94"/>
        <source>Not logged in</source>
        <translation>未登录</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="157"/>
        <source>mini model</source>
        <translation>精简模式</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="166"/>
        <source>To minimize the</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="188"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="667"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="934"/>
        <location filename="../UI/mainwidget.cpp" line="1209"/>
        <source>reduction</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="940"/>
        <location filename="../UI/mainwidget.cpp" line="1201"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1419"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="33"/>
        <source>Menu</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="45"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="47"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="150"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="49"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="148"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="146"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="58"/>
        <source>Auto</source>
        <translation>跟随主题</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="63"/>
        <source>Light</source>
        <translation>浅色</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="68"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="205"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="237"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="278"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation type="vanished">音乐播放器是用于播放各种音乐格式的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="206"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="282"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
</context>
<context>
    <name>miniWidget</name>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="374"/>
        <location filename="../UI/player/miniwidget.cpp" line="739"/>
        <location filename="../UI/player/miniwidget.cpp" line="866"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="375"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="74"/>
        <location filename="../UI/player/miniwidget.cpp" line="439"/>
        <location filename="../UI/player/miniwidget.cpp" line="844"/>
        <source>CurrentItemInLoop</source>
        <translation>单曲循环</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="54"/>
        <source>kylin-music mini</source>
        <translation type="unfinished">音乐</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="73"/>
        <source>Sequence</source>
        <translation>顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="218"/>
        <location filename="../UI/player/miniwidget.cpp" line="282"/>
        <location filename="../UI/player/miniwidget.cpp" line="578"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="231"/>
        <location filename="../UI/player/miniwidget.cpp" line="244"/>
        <location filename="../UI/player/miniwidget.cpp" line="295"/>
        <location filename="../UI/player/miniwidget.cpp" line="308"/>
        <location filename="../UI/player/miniwidget.cpp" line="587"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="402"/>
        <source>Previous</source>
        <translation>上一首</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="411"/>
        <source>Next</source>
        <translation>下一首</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="426"/>
        <source>Favourite</source>
        <translation>喜欢</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="472"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="484"/>
        <source>normal mode</source>
        <translation>正常模式</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="634"/>
        <location filename="../UI/player/miniwidget.cpp" line="650"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="70"/>
        <location filename="../UI/player/miniwidget.cpp" line="71"/>
        <location filename="../UI/player/miniwidget.cpp" line="447"/>
        <location filename="../UI/player/miniwidget.cpp" line="849"/>
        <source>Loop</source>
        <translation>列表循环</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="72"/>
        <location filename="../UI/player/miniwidget.cpp" line="455"/>
        <location filename="../UI/player/miniwidget.cpp" line="854"/>
        <source>Random</source>
        <translation>随机播放</translation>
    </message>
</context>
</TS>
