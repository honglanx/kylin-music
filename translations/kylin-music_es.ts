<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>CustomToolButton</name>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="216"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="226"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="217"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="228"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="218"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="229"/>
        <source>Rename</source>
        <translation>Rebautizar</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="219"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="231"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>Song List</source>
        <translation>Lista de canciones</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>I Love</source>
        <translation>Me encanta</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="34"/>
        <location filename="../UI/player/miniWidget.cpp" line="330"/>
        <location filename="../UI/player/miniWidget.cpp" line="674"/>
        <location filename="../UI/player/miniWidget.cpp" line="799"/>
        <source>Music Player</source>
        <translation>Reproductor de música</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="49"/>
        <location filename="../UI/player/miniWidget.cpp" line="50"/>
        <location filename="../UI/player/miniWidget.cpp" line="391"/>
        <location filename="../UI/player/miniWidget.cpp" line="782"/>
        <source>Loop</source>
        <translation>Bucle</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="51"/>
        <location filename="../UI/player/miniWidget.cpp" line="399"/>
        <location filename="../UI/player/miniWidget.cpp" line="787"/>
        <source>Random</source>
        <translation>Aleatorio</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="52"/>
        <source>Sequence</source>
        <translation>Secuencia</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="331"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="53"/>
        <location filename="../UI/player/miniWidget.cpp" line="383"/>
        <location filename="../UI/player/miniWidget.cpp" line="777"/>
        <source>CurrentItemInLoop</source>
        <translation>CurrentItemInLoop</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="509"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="500"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation type="vanished">喜欢</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="414"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="422"/>
        <source>Maximize</source>
        <translation>Maximizar</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="553"/>
        <location filename="../UI/player/miniWidget.cpp" line="568"/>
        <source>I Love</source>
        <translation>Me encanta</translation>
    </message>
</context>
<context>
    <name>MusicDataBase</name>
    <message>
        <source>Prompt information</source>
        <translation type="obsolete">提示信息</translation>
    </message>
    <message>
        <source>Single song name is already exists</source>
        <translation type="obsolete">歌单名已存在</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="278"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="332"/>
        <source>Unknown singer</source>
        <translation>Cantante desconocido</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="279"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="338"/>
        <source>Unknown album</source>
        <translation>Álbum desconocido</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Prompt information</source>
        <translation>Información rápida</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Add failed, no valid music file found</source>
        <translation>Error al agregar un archivo, no se encontró un archivo de música válido</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="204"/>
        <source>Music Information</source>
        <translation>Información musical</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="179"/>
        <source>Music Player</source>
        <translation>Reproductor de música</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="225"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="227"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="257"/>
        <source>Song Name : </source>
        <translation>Nombre de la canción: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="258"/>
        <source>Singer : </source>
        <translation>Cantante: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="259"/>
        <source>Album : </source>
        <translation>Álbum: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="260"/>
        <source>File Type : </source>
        <translation>Tipo de archivo : </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="261"/>
        <source>File Size : </source>
        <translation>Tamaño del archivo : </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="262"/>
        <source>File Time : </source>
        <translation>Tiempo de archivo : </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="263"/>
        <source>File Path : </source>
        <translation>Ruta del archivo: </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Song</source>
        <translation>Canción</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Singer</source>
        <translation>Cantante</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Album</source>
        <translation>Álbum</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="11"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>Bucle</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>Aleatorio</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>Secuencial</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>CurrentItemInLoop</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="38"/>
        <source>Previous</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="43"/>
        <location filename="../UI/player/playsongarea.cpp" line="566"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="47"/>
        <source>Next</source>
        <translation>Próximo</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="52"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="107"/>
        <source>Favourite</source>
        <translation>Favorito</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="131"/>
        <location filename="../UI/player/playsongarea.cpp" line="445"/>
        <source>Loop</source>
        <translation>Bucle</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="156"/>
        <source>Play List</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="172"/>
        <location filename="../UI/player/playsongarea.cpp" line="476"/>
        <location filename="../UI/player/playsongarea.cpp" line="478"/>
        <location filename="../UI/player/playsongarea.cpp" line="628"/>
        <location filename="../UI/player/playsongarea.cpp" line="998"/>
        <location filename="../UI/player/playsongarea.cpp" line="1023"/>
        <source>Music Player</source>
        <translation>Reproductor de música</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="173"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="391"/>
        <location filename="../UI/player/playsongarea.cpp" line="403"/>
        <source>I Love</source>
        <translation>Me encanta</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="556"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="139"/>
        <location filename="../UI/player/playsongarea.cpp" line="450"/>
        <source>Random</source>
        <translation>Aleatorio</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="123"/>
        <location filename="../UI/player/playsongarea.cpp" line="440"/>
        <source>CurrentItemInLoop</source>
        <translation>CurrentItemInLoop</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <location filename="../UI/base/popupDialog.cpp" line="82"/>
        <source>Prompt information</source>
        <translation>Información rápida</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>No podía contener caracteres: / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="55"/>
        <source>Reached upper character limit</source>
        <translation>Se ha alcanzado el límite superior de caracteres</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="64"/>
        <source>Music Player</source>
        <translation>Reproductor de música</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="109"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="113"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="121"/>
        <source>Please input playlist name:</source>
        <translation>Introduzca el nombre de la lista de reproducción:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="60"/>
        <source>Database Error</source>
        <translation>Error de base de datos</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="111"/>
        <source>Search Result</source>
        <translation>Resultado de la búsqueda</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="29"/>
        <source>Music</source>
        <translation>Música</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="45"/>
        <source>Singer</source>
        <translation>Cantante</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="61"/>
        <source>Album</source>
        <translation>Álbum</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="56"/>
        <source>Music Player</source>
        <translation>Reproductor de música</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="74"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="81"/>
        <source>Song List</source>
        <translation>Lista de canciones</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="104"/>
        <source>My PlayList</source>
        <translation>Mi lista de reproducción</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="133"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="347"/>
        <source>New Playlist</source>
        <translation>Nueva lista de reproducción</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="137"/>
        <source>Rename</source>
        <translation>Rebautizar</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="244"/>
        <source>I Love</source>
        <translation>Me encanta</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="426"/>
        <source>Prompt information</source>
        <translation>Información rápida</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="426"/>
        <source>Single song name already exists!!!</source>
        <translation>El nombre de una sola canción ya existe!!</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="49"/>
        <source>History</source>
        <translation>Historia</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="56"/>
        <source>Empty</source>
        <translation>Vacío</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="80"/>
        <source>The playlist has no songs</source>
        <translation>La lista de reproducción no tiene canciones</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Prompt information</source>
        <translation>Información rápida</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Clear the playlist?</source>
        <translation>¿Borrar la lista de reproducción?</translation>
    </message>
    <message>
        <source>song</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <source>total</source>
        <translation type="vanished">共</translation>
    </message>
    <message>
        <source>songs</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="433"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="461"/>
        <source>path does not exist</source>
        <translation>La ruta de acceso no existe</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <source> songs</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="327"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="328"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="329"/>
        <source>Play the next one</source>
        <translation>Juega el siguiente</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="284"/>
        <location filename="../UI/tableview/tableone.cpp" line="1091"/>
        <location filename="../UI/tableview/tableone.cpp" line="1150"/>
        <location filename="../UI/tableview/tableone.cpp" line="1223"/>
        <source>Song List</source>
        <translation>Lista de canciones</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="211"/>
        <source>There are no songs!</source>
        <translation>¡No hay canciones!</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="154"/>
        <source>Play All</source>
        <translation>Jugar todo</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <source>Add Music</source>
        <translation>Añadir música</translation>
    </message>
    <message>
        <source>Add local songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add local folders</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <source>Add Local Songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add Local Folder</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="163"/>
        <location filename="../UI/tableview/tableone.cpp" line="212"/>
        <source>Open File</source>
        <translation>Abrir archivo</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="166"/>
        <location filename="../UI/tableview/tableone.cpp" line="213"/>
        <source>Open Folder</source>
        <translation>Abrir carpeta</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="286"/>
        <location filename="../UI/tableview/tableone.cpp" line="359"/>
        <location filename="../UI/tableview/tableone.cpp" line="642"/>
        <location filename="../UI/tableview/tableone.cpp" line="1093"/>
        <location filename="../UI/tableview/tableone.cpp" line="1152"/>
        <source>I Love</source>
        <translation>Me encanta</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="288"/>
        <location filename="../UI/tableview/tableone.cpp" line="1095"/>
        <location filename="../UI/tableview/tableone.cpp" line="1154"/>
        <location filename="../UI/tableview/tableone.cpp" line="1200"/>
        <location filename="../UI/tableview/tableone.cpp" line="1249"/>
        <location filename="../UI/tableview/tableone.cpp" line="1278"/>
        <source>Search Result</source>
        <translation>Resultado de la búsqueda</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="317"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="318"/>
        <source>Delete from list</source>
        <translation>Eliminar de la lista</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="319"/>
        <source>Remove from local</source>
        <translation>Quitar de local</translation>
    </message>
    <message>
        <source>Delete Selected Items</source>
        <translation type="vanished">删除选中项</translation>
    </message>
    <message>
        <source>Song Information</source>
        <translation type="vanished">歌曲信息</translation>
    </message>
    <message>
        <source>Add To Songlist</source>
        <translation type="vanished">添加歌曲到</translation>
    </message>
    <message>
        <source>Open The Folder</source>
        <translation type="vanished">打开所在文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="389"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>¿Confirmar que la canción seleccionada se eliminará de la lista de canciones?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="415"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>Una vez que la canción se elimina del local, no se puede reanudar. ¿Es seguro que se eliminará?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="723"/>
        <source>Audio File</source>
        <translation>Archivo de audio</translation>
    </message>
    <message>
        <source>Are you sure to clear the list?</source>
        <translation type="vanished">确认清空歌单列表？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="896"/>
        <source>Add failed! Format error or no access permission!</source>
        <translation>¡Agregar falló! ¡Error de formato o falta de permiso de acceso!</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="905"/>
        <location filename="../UI/tableview/tableone.cpp" line="934"/>
        <source>Success add %1 song</source>
        <translation>Éxito añadir %1 canción</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="939"/>
        <source>Repeat add</source>
        <translation>Repetir adición</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="978"/>
        <source>path does not exist</source>
        <translation>La ruta de acceso no existe</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1020"/>
        <source> song</source>
        <translation> canción</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <source>Success add %1 songs</source>
        <translation type="vanished">成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="916"/>
        <source>Add failed</source>
        <translation>Error en la adición</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="320"/>
        <source>View song information</source>
        <translation>Ver información de la canción</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="321"/>
        <source>Add to songlist</source>
        <translation>Añadir a la lista de canciones</translation>
    </message>
    <message>
        <source>Clear List</source>
        <translation type="vanished">清空列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="389"/>
        <location filename="../UI/tableview/tableone.cpp" line="415"/>
        <location filename="../UI/tableview/tableone.cpp" line="905"/>
        <location filename="../UI/tableview/tableone.cpp" line="916"/>
        <location filename="../UI/tableview/tableone.cpp" line="934"/>
        <location filename="../UI/tableview/tableone.cpp" line="939"/>
        <source>Prompt information</source>
        <translation>Información rápida</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1022"/>
        <source> songs</source>
        <translation> Canciones</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="57"/>
        <source>back</source>
        <translation>Atrás</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="68"/>
        <source>forward</source>
        <translation>adelante</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="91"/>
        <source>Not logged in</source>
        <translation>No ha iniciado sesión</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="159"/>
        <source>mini model</source>
        <translation>Modelo mini</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="167"/>
        <source>To minimize the</source>
        <translation>Para minimizar el</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>maximizar</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="186"/>
        <source>close</source>
        <translation>cerrar</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="675"/>
        <source>Music Player</source>
        <translation>Reproductor de música</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="962"/>
        <location filename="../UI/mainwidget.cpp" line="1260"/>
        <source>reduction</source>
        <translation>reducción</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="966"/>
        <location filename="../UI/mainwidget.cpp" line="1247"/>
        <source>maximize</source>
        <translation>maximizar</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1428"/>
        <source>Prompt information</source>
        <translation>Información rápida</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">选项</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="24"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="37"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="39"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="125"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="41"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="123"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="43"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="121"/>
        <source>Exit</source>
        <translation>Salida</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <source>Auto</source>
        <translation>Automático</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="56"/>
        <source>Light</source>
        <translation>Luz</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="59"/>
        <source>Dark</source>
        <translation>Oscuro</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">音乐</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="189"/>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation>El reproductor de música es un software de reproducción multimedia. Cubre varios formatos de música Herramientas de reproducción para rápido y simple.</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="188"/>
        <source>Version: </source>
        <translation>Versión: </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.h" line="63"/>
        <source>kylin-music</source>
        <translation>kylin-música</translation>
    </message>
</context>
</TS>
