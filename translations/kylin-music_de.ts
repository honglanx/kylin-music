<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>CustomToolButton</name>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="216"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="226"/>
        <source>Play</source>
        <translation>Spielen</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="217"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="228"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="218"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="229"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="219"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="231"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>Song List</source>
        <translation>Songliste</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>I Love</source>
        <translation>Ich liebe</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="34"/>
        <location filename="../UI/player/miniWidget.cpp" line="330"/>
        <location filename="../UI/player/miniWidget.cpp" line="674"/>
        <location filename="../UI/player/miniWidget.cpp" line="799"/>
        <source>Music Player</source>
        <translation>Musik-Player</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="49"/>
        <location filename="../UI/player/miniWidget.cpp" line="50"/>
        <location filename="../UI/player/miniWidget.cpp" line="391"/>
        <location filename="../UI/player/miniWidget.cpp" line="782"/>
        <source>Loop</source>
        <translation>Schleife</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="51"/>
        <location filename="../UI/player/miniWidget.cpp" line="399"/>
        <location filename="../UI/player/miniWidget.cpp" line="787"/>
        <source>Random</source>
        <translation>Zufällig</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="52"/>
        <source>Sequence</source>
        <translation>Reihenfolge</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="331"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="53"/>
        <location filename="../UI/player/miniWidget.cpp" line="383"/>
        <location filename="../UI/player/miniWidget.cpp" line="777"/>
        <source>CurrentItemInLoop</source>
        <translation>CurrentItemInLoop</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="509"/>
        <source>Play</source>
        <translation>Spielen</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="500"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation type="vanished">喜欢</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="414"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="422"/>
        <source>Maximize</source>
        <translation>Maximieren</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="553"/>
        <location filename="../UI/player/miniWidget.cpp" line="568"/>
        <source>I Love</source>
        <translation>Ich liebe</translation>
    </message>
</context>
<context>
    <name>MusicDataBase</name>
    <message>
        <source>Prompt information</source>
        <translation type="obsolete">提示信息</translation>
    </message>
    <message>
        <source>Single song name is already exists</source>
        <translation type="obsolete">歌单名已存在</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="278"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="332"/>
        <source>Unknown singer</source>
        <translation>Unbekannter Sänger</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="279"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="338"/>
        <source>Unknown album</source>
        <translation>Unbekanntes Album</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Prompt information</source>
        <translation>Schnelle Informationen</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Add failed, no valid music file found</source>
        <translation>Hinzufügen fehlgeschlagen, keine gültige Musikdatei gefunden</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="204"/>
        <source>Music Information</source>
        <translation>Informationen zur Musik</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="179"/>
        <source>Music Player</source>
        <translation>Musik-Player</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="225"/>
        <source>Confirm</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="227"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="257"/>
        <source>Song Name : </source>
        <translation>Name des Liedes : </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="258"/>
        <source>Singer : </source>
        <translation>Sänger: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="259"/>
        <source>Album : </source>
        <translation>Album: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="260"/>
        <source>File Type : </source>
        <translation>Dateityp: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="261"/>
        <source>File Size : </source>
        <translation>Dateigröße: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="262"/>
        <source>File Time : </source>
        <translation>Datei-Zeit : </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="263"/>
        <source>File Path : </source>
        <translation>Dateipfad: </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Song</source>
        <translation>Lied</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Singer</source>
        <translation>Sänger</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="11"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>Schleife</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>Zufällig</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>Sequenziell</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>CurrentItemInLoop</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="38"/>
        <source>Previous</source>
        <translation>Vorhergehend</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="43"/>
        <location filename="../UI/player/playsongarea.cpp" line="566"/>
        <source>Play</source>
        <translation>Spielen</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="47"/>
        <source>Next</source>
        <translation>Nächster</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="52"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="107"/>
        <source>Favourite</source>
        <translation>Favorit</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="131"/>
        <location filename="../UI/player/playsongarea.cpp" line="445"/>
        <source>Loop</source>
        <translation>Schleife</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="156"/>
        <source>Play List</source>
        <translation>Wiedergabeliste</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="172"/>
        <location filename="../UI/player/playsongarea.cpp" line="476"/>
        <location filename="../UI/player/playsongarea.cpp" line="478"/>
        <location filename="../UI/player/playsongarea.cpp" line="628"/>
        <location filename="../UI/player/playsongarea.cpp" line="998"/>
        <location filename="../UI/player/playsongarea.cpp" line="1023"/>
        <source>Music Player</source>
        <translation>Musik-Player</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="173"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="391"/>
        <location filename="../UI/player/playsongarea.cpp" line="403"/>
        <source>I Love</source>
        <translation>Ich liebe</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="556"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="139"/>
        <location filename="../UI/player/playsongarea.cpp" line="450"/>
        <source>Random</source>
        <translation>Zufällig</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="123"/>
        <location filename="../UI/player/playsongarea.cpp" line="440"/>
        <source>CurrentItemInLoop</source>
        <translation>CurrentItemInLoop</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <location filename="../UI/base/popupDialog.cpp" line="82"/>
        <source>Prompt information</source>
        <translation>Schnelle Informationen</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>Konnte keine Zeichen enthalten: / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="55"/>
        <source>Reached upper character limit</source>
        <translation>Obergrenze für Zeichen erreicht</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="64"/>
        <source>Music Player</source>
        <translation>Musik-Player</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="109"/>
        <source>Confirm</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="113"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="121"/>
        <source>Please input playlist name:</source>
        <translation>Bitte geben Sie den Namen der Wiedergabeliste ein:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="60"/>
        <source>Database Error</source>
        <translation>Datenbank-Fehler</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="111"/>
        <source>Search Result</source>
        <translation>Suchergebnis</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="29"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="45"/>
        <source>Singer</source>
        <translation>Sänger</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="61"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="56"/>
        <source>Music Player</source>
        <translation>Musik-Player</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="74"/>
        <source>Library</source>
        <translation>Bibliothek</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="81"/>
        <source>Song List</source>
        <translation>Songliste</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="104"/>
        <source>My PlayList</source>
        <translation>Meine Wiedergabeliste</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="133"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="347"/>
        <source>New Playlist</source>
        <translation>Neue Playlist</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="137"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="244"/>
        <source>I Love</source>
        <translation>Ich liebe</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="426"/>
        <source>Prompt information</source>
        <translation>Schnelle Informationen</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="426"/>
        <source>Single song name already exists!!!</source>
        <translation>Der Name eines einzelnen Songs existiert bereits!!</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="49"/>
        <source>History</source>
        <translation>Geschichte</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="56"/>
        <source>Empty</source>
        <translation>Leer</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="80"/>
        <source>The playlist has no songs</source>
        <translation>Die Playlist enthält keine Songs</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Prompt information</source>
        <translation>Schnelle Informationen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Clear the playlist?</source>
        <translation>Die Playlist leeren?</translation>
    </message>
    <message>
        <source>song</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <source>total</source>
        <translation type="vanished">共</translation>
    </message>
    <message>
        <source>songs</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="433"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="461"/>
        <source>path does not exist</source>
        <translation>Pfad existiert nicht</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <source> songs</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="327"/>
        <source>Play</source>
        <translation>Spielen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="328"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="329"/>
        <source>Play the next one</source>
        <translation>Spielen Sie das nächste Spiel</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="284"/>
        <location filename="../UI/tableview/tableone.cpp" line="1091"/>
        <location filename="../UI/tableview/tableone.cpp" line="1150"/>
        <location filename="../UI/tableview/tableone.cpp" line="1223"/>
        <source>Song List</source>
        <translation>Songliste</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="211"/>
        <source>There are no songs!</source>
        <translation>Es gibt keine Songs!</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="154"/>
        <source>Play All</source>
        <translation>Alle abspielen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <source>Add Music</source>
        <translation>Musik hinzufügen</translation>
    </message>
    <message>
        <source>Add local songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add local folders</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <source>Add Local Songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add Local Folder</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="163"/>
        <location filename="../UI/tableview/tableone.cpp" line="212"/>
        <source>Open File</source>
        <translation>Offene Linie</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="166"/>
        <location filename="../UI/tableview/tableone.cpp" line="213"/>
        <source>Open Folder</source>
        <translation>Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="286"/>
        <location filename="../UI/tableview/tableone.cpp" line="359"/>
        <location filename="../UI/tableview/tableone.cpp" line="642"/>
        <location filename="../UI/tableview/tableone.cpp" line="1093"/>
        <location filename="../UI/tableview/tableone.cpp" line="1152"/>
        <source>I Love</source>
        <translation>Ich liebe</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="288"/>
        <location filename="../UI/tableview/tableone.cpp" line="1095"/>
        <location filename="../UI/tableview/tableone.cpp" line="1154"/>
        <location filename="../UI/tableview/tableone.cpp" line="1200"/>
        <location filename="../UI/tableview/tableone.cpp" line="1249"/>
        <location filename="../UI/tableview/tableone.cpp" line="1278"/>
        <source>Search Result</source>
        <translation>Suchergebnis</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="317"/>
        <source>Play</source>
        <translation>Spielen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="318"/>
        <source>Delete from list</source>
        <translation>Aus Liste löschen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="319"/>
        <source>Remove from local</source>
        <translation>Aus lokalem entfernen</translation>
    </message>
    <message>
        <source>Delete Selected Items</source>
        <translation type="vanished">删除选中项</translation>
    </message>
    <message>
        <source>Song Information</source>
        <translation type="vanished">歌曲信息</translation>
    </message>
    <message>
        <source>Add To Songlist</source>
        <translation type="vanished">添加歌曲到</translation>
    </message>
    <message>
        <source>Open The Folder</source>
        <translation type="vanished">打开所在文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="389"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>Bestätigen Sie, dass der ausgewählte Song aus der Songliste gelöscht wird?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="415"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>Nachdem der Song aus dem lokalen Speicher gelöscht wurde, kann er nicht mehr fortgesetzt werden. Ist es sicher, dass es gelöscht wird?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="723"/>
        <source>Audio File</source>
        <translation>Audiodatei</translation>
    </message>
    <message>
        <source>Are you sure to clear the list?</source>
        <translation type="vanished">确认清空歌单列表？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="896"/>
        <source>Add failed! Format error or no access permission!</source>
        <translation>Hinzufügen fehlgeschlagen! Formatfehler oder keine Zugriffsberechtigung!</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="905"/>
        <location filename="../UI/tableview/tableone.cpp" line="934"/>
        <source>Success add %1 song</source>
        <translation>Erfolg Hinzufügen von %1 Song</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="939"/>
        <source>Repeat add</source>
        <translation>Wiederholen Sie das Hinzufügen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="978"/>
        <source>path does not exist</source>
        <translation>Pfad existiert nicht</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1020"/>
        <source> song</source>
        <translation> Lied</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <source>Success add %1 songs</source>
        <translation type="vanished">成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="916"/>
        <source>Add failed</source>
        <translation>Fehler hinzufügen</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="320"/>
        <source>View song information</source>
        <translation>Song-Informationen anzeigen</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="321"/>
        <source>Add to songlist</source>
        <translation>Zur Songliste hinzufügen</translation>
    </message>
    <message>
        <source>Clear List</source>
        <translation type="vanished">清空列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="389"/>
        <location filename="../UI/tableview/tableone.cpp" line="415"/>
        <location filename="../UI/tableview/tableone.cpp" line="905"/>
        <location filename="../UI/tableview/tableone.cpp" line="916"/>
        <location filename="../UI/tableview/tableone.cpp" line="934"/>
        <location filename="../UI/tableview/tableone.cpp" line="939"/>
        <source>Prompt information</source>
        <translation>Schnelle Informationen</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1022"/>
        <source> songs</source>
        <translation> Lieder</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="57"/>
        <source>back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="68"/>
        <source>forward</source>
        <translation>vorwärts</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="91"/>
        <source>Not logged in</source>
        <translation>Nicht eingeloggt</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="159"/>
        <source>mini model</source>
        <translation>Mini-Modell</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="167"/>
        <source>To minimize the</source>
        <translation>Um die</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>maximieren</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="186"/>
        <source>close</source>
        <translation>schließen</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="675"/>
        <source>Music Player</source>
        <translation>Musik-Player</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="962"/>
        <location filename="../UI/mainwidget.cpp" line="1260"/>
        <source>reduction</source>
        <translation>Reduktion</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="966"/>
        <location filename="../UI/mainwidget.cpp" line="1247"/>
        <source>maximize</source>
        <translation>maximieren</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1428"/>
        <source>Prompt information</source>
        <translation>Schnelle Informationen</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">选项</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="24"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="37"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="39"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="125"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="41"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="123"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="43"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="121"/>
        <source>Exit</source>
        <translation>Ausgang</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="56"/>
        <source>Light</source>
        <translation>Licht</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="59"/>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">音乐</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="189"/>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation>Der Musikplayer ist eine Multimedia-Wiedergabesoftware. Abdeckung Verschiedene Musikformate Wiedergabewerkzeuge für, schnell und einfach.</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="188"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.h" line="63"/>
        <source>kylin-music</source>
        <translation>Kylin-Musik</translation>
    </message>
</context>
</TS>
