<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AllPupWindow</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <location filename="../UI/base/allpupwindow.cpp" line="87"/>
        <source>Prompt information</source>
        <translation type="unfinished">ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation type="unfinished">ᠲᠤᠤᠷᠠᠬᠢ ᠲᠡᠮᠳᠡᠭ ᠤ᠋ᠳ ᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠢᠬᠤ ᠦᠭᠡᠢ :  / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="53"/>
        <source>Reached upper character limit</source>
        <translation type="unfinished">ᠨᠢᠭᠡᠨᠳᠡ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ ᠦ᠋ᠨ ᠳᠡᠭᠡᠳᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠲᠦ᠍ ᠬᠦᠷᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="62"/>
        <source>Music Player</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠦᠨ ᠨᠡᠪᠲᠡᠷᠡᠭᠦᠯᠭᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="115"/>
        <source>Confirm</source>
        <translation type="unfinished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="119"/>
        <source>Cancel</source>
        <translation type="unfinished">ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="128"/>
        <source>Please input playlist name:</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠨ ᠦ᠌ ᠨᠡᠷᠡᠢᠳᠤᠯ ᠢ᠋ ᠤᠷᠤᠯᠠᠭᠠᠷᠠᠢ:</translation>
    </message>
</context>
<context>
    <name>CustomToolButton</name>
    <message>
        <source>Play</source>
        <translation type="vanished">ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠦ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation type="vanished">ᠪᠢ ᠲᠤᠷᠠᠲᠠᠢ</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <source>Music Player</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <source>Loop</source>
        <translation type="vanished">ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠶ᠋ᠢᠨ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="vanished">ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠡ</translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation type="vanished">ᠳᠠᠷᠠᠭᠠᠯᠠᠯ&#x202f;ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>00:00/00:00</source>
        <translation type="vanished">00:00/00:00</translation>
    </message>
    <message>
        <source>CurrentItemInLoop</source>
        <translation type="vanished">ᠳᠠᠨᠭ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation type="vanished">ᠪᠢ ᠲᠤᠷᠠᠲᠠᠢ</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="229"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="265"/>
        <source>Unknown singer</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠳᠠᠭᠤᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="230"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="268"/>
        <source>Unknown album</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠲᠤᠰᠭᠠᠢ ᠹᠢᠯᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Add failed, no valid music file found</source>
        <translation>add failed, no valid music file found</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <source>Music Information</source>
        <translation type="vanished">ᠳᠠᠭᠤᠨ ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="240"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="387"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="389"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="312"/>
        <source>Song Name : </source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠨᠡᠷ᠎ᠡ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="299"/>
        <source>  Music Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="313"/>
        <source>Singer : </source>
        <translation>ᠳᠠᠭᠤᠴᠢᠨ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="314"/>
        <source>Album : </source>
        <translation>ᠲᠤᠰᠭᠠᠢ ᠹᠢᠯᠢᠮ ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="315"/>
        <source>File Type : </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ: </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="316"/>
        <source>File Size : </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ: </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="317"/>
        <source>File Time : </source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠴᠠᠭ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="318"/>
        <source>File Path : </source>
        <translation>ᠹᠠᠢᠯ&#x202f;ᠤ᠋ᠨ ᠵᠢᠮ᠄ </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="7"/>
        <source>Song</source>
        <translation>ᠳᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Singer</source>
        <translation>ᠳᠠᠭᠤᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Album</source>
        <translation>ᠲᠤᠰᠬᠠᠢ ᠹᠢᠯᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Time</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>MyToolButton</name>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="142"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="155"/>
        <source>Play</source>
        <translation type="unfinished">ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="143"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="159"/>
        <source>Pause</source>
        <translation type="unfinished">ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="144"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="163"/>
        <source>Rename</source>
        <translation type="unfinished">ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="145"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="167"/>
        <source>Delete</source>
        <translation type="unfinished">ᠬᠠᠰᠤᠨᠠ᠃</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>Song List</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠤ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠵᠢᠭ᠌ᠰᠠᠭᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>I Love</source>
        <translation type="unfinished">ᠪᠢ ᠬᠠᠢ᠌ᠷᠠᠲᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠶ᠋ᠢᠨ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠵᠢᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>ᠳᠠᠨᠭ ᠣᠷᠴᠢᠯ</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="32"/>
        <source>Previous</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠲᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="617"/>
        <location filename="../UI/player/playsongarea.cpp" line="989"/>
        <location filename="../UI/player/playsongarea.cpp" line="1001"/>
        <location filename="../UI/player/playsongarea.cpp" line="1045"/>
        <location filename="../UI/player/playsongarea.cpp" line="1057"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="41"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="45"/>
        <source>Volume</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="106"/>
        <source>Favourite</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="133"/>
        <location filename="../UI/player/playsongarea.cpp" line="149"/>
        <location filename="../UI/player/playsongarea.cpp" line="486"/>
        <location filename="../UI/player/playsongarea.cpp" line="532"/>
        <source>Loop</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠶ᠋ᠢᠨ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="168"/>
        <source>Play List</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠦᠭᠰᠡᠨ ᠲᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="188"/>
        <location filename="../UI/player/playsongarea.cpp" line="558"/>
        <location filename="../UI/player/playsongarea.cpp" line="560"/>
        <location filename="../UI/player/playsongarea.cpp" line="673"/>
        <location filename="../UI/player/playsongarea.cpp" line="906"/>
        <location filename="../UI/player/playsongarea.cpp" line="934"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="189"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="429"/>
        <location filename="../UI/player/playsongarea.cpp" line="446"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="607"/>
        <location filename="../UI/player/playsongarea.cpp" line="977"/>
        <location filename="../UI/player/playsongarea.cpp" line="1033"/>
        <source>Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="141"/>
        <location filename="../UI/player/playsongarea.cpp" line="151"/>
        <location filename="../UI/player/playsongarea.cpp" line="495"/>
        <location filename="../UI/player/playsongarea.cpp" line="537"/>
        <source>Random</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠡ</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="125"/>
        <location filename="../UI/player/playsongarea.cpp" line="153"/>
        <location filename="../UI/player/playsongarea.cpp" line="512"/>
        <location filename="../UI/player/playsongarea.cpp" line="522"/>
        <source>CurrentItemInLoop</source>
        <translation>ᠳᠠᠨᠭ ᠣᠷᠴᠢᠯ</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <source>Prompt information</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation type="vanished">ᠲᠤᠤᠷᠠᠬᠢ ᠲᠡᠮᠳᠡᠭ ᠤ᠋ᠳ ᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠢᠬᠤ ᠦᠭᠡᠢ :  / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <source>Reached upper character limit</source>
        <translation type="vanished">ᠨᠢᠭᠡᠨᠳᠡ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ ᠦ᠋ᠨ ᠳᠡᠭᠡᠳᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠲᠦ᠍ ᠬᠦᠷᠪᠡ</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Please input playlist name:</source>
        <translation type="vanished">ᠳᠠᠭᠤᠨ ᠦ᠌ ᠨᠡᠷᠡᠢᠳᠤᠯ ᠢ᠋ ᠤᠷᠤᠯᠠᠭᠠᠷᠠᠢ:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="64"/>
        <source>Database Error</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠬᠦᠮᠦᠷᠬᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="120"/>
        <source>Search Result</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠦᠷᠡ ᠳ᠋ᠦᠨᠭ</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="34"/>
        <source>Music</source>
        <translation>ᠳᠠᠭᠤᠤ ᠭᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="50"/>
        <source>Singer</source>
        <translation>ᠳᠠᠭᠤᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="66"/>
        <source>Album</source>
        <translation>ᠲᠤᠰᠬᠠᠢ ᠹᠢᠯᠢᠮ</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="58"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="88"/>
        <source>Library</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠦᠮᠦᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="97"/>
        <source>Song List</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠦ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="109"/>
        <source>My PlayList</source>
        <translation>ᠮᠢᠨᠤ ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="154"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="363"/>
        <source>New Playlist</source>
        <translation>ᠰᠢᠨᠡ ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="158"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="268"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Single song name already exists!!!</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠷᠤᠰᠢᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">ᠲᠡᠦᠬᠡ ᠶᠢᠨ ᠫᠯᠧᠰᠲ᠋ ᠃</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠨᠡᠪᠲᠡᠷᠡ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="44"/>
        <source>History</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠦᠭᠰᠡᠨ ᠲᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="52"/>
        <source>Empty</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="70"/>
        <source>The playlist has no songs</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠳ᠋ᠦ᠍ ᠪᠠᠰᠠ ᠲᠠᠭᠤᠤ ᠦᠭᠡᠢ ᠪᠠᠢᠨ᠎ᠠ ᠰᠢᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="356"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <source>Clear the playlist?</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="131"/>
        <source>kylin-music</source>
        <translation type="unfinished">ᠵᠢᠢ ᠯᠢᠨ ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="356"/>
        <source>The file path does not exist or has been deleted</source>
        <translation type="unfinished">ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠷᠭᠠ ᠵᠠᠮ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠠᠰᠤᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="479"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="507"/>
        <source>path does not exist</source>
        <translation>ᠵᠠᠮ ᠮᠥᠷ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">ᠶᠡᠷᠦᠩᠬᠡᠢ ᠳᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="265"/>
        <source> songs</source>
        <translation> ᠨᠠᠷᠭᠢᠶ ᠠ ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="294"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="295"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="296"/>
        <source>Play the next one</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠠᠭᠤᠤ ᠵᠢ ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">ᠡᠨᠡ ᠳᠠᠭᠤᠤ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">ᠨᠡᠮᠡ ᠃</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ᠃</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">ᠳᠠᠭᠤᠴᠢᠨ ᠃</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">ᠺᠢᠨᠣ᠋ ᠶᠢᠨ ᠪᠥᠯᠥᠭ᠍</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">ᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="260"/>
        <location filename="../UI/tableview/tableone.cpp" line="1129"/>
        <location filename="../UI/tableview/tableone.cpp" line="1170"/>
        <location filename="../UI/tableview/tableone.cpp" line="1242"/>
        <source>Song List</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="193"/>
        <source>There are no songs!</source>
        <translation>ᠪᠠᠰᠠ ᠳᠠᠭᠤᠤ ᠦᠭᠡᠢ ᠪᠠᠢᠨᠠ!</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="149"/>
        <source>Play All</source>
        <translation>ᠪᠦᠬᠦᠳᠡ ᠵᠢ ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="152"/>
        <source>Add Music</source>
        <translation>ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="158"/>
        <source>Add local songs</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="159"/>
        <source>Add local folders</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠠᠵᠠᠭᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="194"/>
        <source>Add Local Songs</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="195"/>
        <source>Add Local Folder</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠠᠵᠠᠭᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">ᠹᠠᠢᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation type="vanished">ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠢᠳᠠᠰᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="262"/>
        <location filename="../UI/tableview/tableone.cpp" line="345"/>
        <location filename="../UI/tableview/tableone.cpp" line="718"/>
        <location filename="../UI/tableview/tableone.cpp" line="1131"/>
        <location filename="../UI/tableview/tableone.cpp" line="1172"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="264"/>
        <location filename="../UI/tableview/tableone.cpp" line="1133"/>
        <location filename="../UI/tableview/tableone.cpp" line="1174"/>
        <location filename="../UI/tableview/tableone.cpp" line="1219"/>
        <location filename="../UI/tableview/tableone.cpp" line="1268"/>
        <location filename="../UI/tableview/tableone.cpp" line="1297"/>
        <source>Search Result</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠨᠭ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="292"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="293"/>
        <source>Delete from list</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠡᠴᠡ ᠬᠠᠰᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="294"/>
        <source>Remove from local</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠠᠴᠠ ᠬᠠᠰᠤᠨᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="369"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠳᠠᠭᠤᠤ ᠪᠠᠨ ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="371"/>
        <location filename="../UI/tableview/tableone.cpp" line="531"/>
        <location filename="../UI/tableview/tableone.cpp" line="635"/>
        <location filename="../UI/tableview/tableone.cpp" line="748"/>
        <location filename="../UI/tableview/tableone.cpp" line="828"/>
        <location filename="../UI/tableview/tableone.cpp" line="957"/>
        <location filename="../UI/tableview/tableone.cpp" line="968"/>
        <location filename="../UI/tableview/tableone.cpp" line="982"/>
        <location filename="../UI/tableview/tableone.cpp" line="992"/>
        <source>kylin-music</source>
        <translation type="unfinished">ᠵᠢᠢ ᠯᠢᠨ ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="374"/>
        <location filename="../UI/tableview/tableone.cpp" line="403"/>
        <location filename="../UI/tableview/tableone.cpp" line="532"/>
        <location filename="../UI/tableview/tableone.cpp" line="636"/>
        <location filename="../UI/tableview/tableone.cpp" line="969"/>
        <location filename="../UI/tableview/tableone.cpp" line="993"/>
        <source>Yes</source>
        <translation type="unfinished">ᠲᠡᠢ᠌ᠮᠦ ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="375"/>
        <location filename="../UI/tableview/tableone.cpp" line="404"/>
        <location filename="../UI/tableview/tableone.cpp" line="533"/>
        <location filename="../UI/tableview/tableone.cpp" line="637"/>
        <source>No</source>
        <translation type="unfinished">ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="399"/>
        <location filename="../UI/tableview/tableone.cpp" line="529"/>
        <location filename="../UI/tableview/tableone.cpp" line="633"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>ᠳᠠᠭᠤᠤ ᠵᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ᠂ ᠨᠡᠬᠡᠷᠡᠨ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="798"/>
        <source>Audio File</source>
        <translation>ᠳᠠᠭᠤᠨ&#x202f;ᠤ᠋ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="947"/>
        <source>This format file is not supported</source>
        <translation type="unfinished">iᠡᠨᠡ ᠬᠡᠪ ᠵᠠᠭᠪᠤᠷ ᠤᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Add failed! Format error or no access permission!</source>
        <translation type="vanished">ᠲᠤᠰ ᠬᠡᠯᠡᠪᠷᠢ ᠵᠢᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Success add %1 song</source>
        <translation type="vanished">ᠠᠮᠵᠢᠯᠲᠠ ᠲᠠᠢ ᠪᠠᠷ 1 ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="991"/>
        <source>Repeat add</source>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠠᠪᠳᠠᠨ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1030"/>
        <source>path does not exist</source>
        <translation>ᠵᠢᠮ ᠨᠢ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> ᠲᠠᠷᠢᠭᠦᠨ ᠦ᠌ ᠳᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">ᠲᠠ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠲᠤ ᠲᠡᠭᠦᠨ ᠢ ᠬᠠᠰᠤᠬᠤ ᠭᠡᠵᠦ ᠲᠣᠭᠲᠠᠭᠠᠨᠠ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="956"/>
        <location filename="../UI/tableview/tableone.cpp" line="981"/>
        <source>Success add %1 songs</source>
        <translation>ᠠᠮᠵᠢᠯᠲᠠ ᠲᠠᠢ ᠪᠠᠷ 1 ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="967"/>
        <source>Add failed</source>
        <translation>ᠨᠡᠮᠡᠵᠦ ᠲᠡᠢᠯᠦᠭᠰᠡᠨ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">ᠬᠠᠰᠤᠨᠠ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="295"/>
        <source>View song information</source>
        <translation>ᠳᠠᠭᠤᠤ ᠶᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠶᠢ ᠦᠵᠡᠨᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="296"/>
        <source>Add to songlist</source>
        <translation>ᠳᠠᠭᠤᠤ ᠶᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠳᠤ ᠨᠡᠮᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="369"/>
        <location filename="../UI/tableview/tableone.cpp" line="399"/>
        <location filename="../UI/tableview/tableone.cpp" line="529"/>
        <location filename="../UI/tableview/tableone.cpp" line="633"/>
        <location filename="../UI/tableview/tableone.cpp" line="956"/>
        <location filename="../UI/tableview/tableone.cpp" line="967"/>
        <location filename="../UI/tableview/tableone.cpp" line="981"/>
        <location filename="../UI/tableview/tableone.cpp" line="991"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ !</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ !</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">ᠶᠡᠷᠦᠩᠬᠡᠢ ᠳᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1067"/>
        <source> songs</source>
        <translation> ᠲᠠᠷᠢᠭᠦᠨ ᠦ᠌ ᠳᠠᠭᠤᠤ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="58"/>
        <source>back</source>
        <translation>ᠡᠮᠦᠨ᠎ᠡ ᠠᠯᠬᠤᠮ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="70"/>
        <source>forward</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠂ ᠳᠠᠭᠤᠴᠢᠨ ᠢ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="79"/>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="94"/>
        <source>Not logged in</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">ᠵᠠᠭᠤᠰᠢ ᠶᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ᠃</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">ᠵᠠᠭᠤᠰᠢ ᠶᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ᠃</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="157"/>
        <source>mini model</source>
        <translation>ᠲᠦᠭᠦᠮᠴᠢᠯᠡᠭᠰᠡᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="166"/>
        <source>To minimize the</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="188"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="667"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠵᠢᠭ᠌ᠰᠠᠭᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="934"/>
        <location filename="../UI/mainwidget.cpp" line="1209"/>
        <source>reduction</source>
        <translation>ᠤᠤᠯ ᠬᠡᠪ ᠲᠦ᠍ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="940"/>
        <location filename="../UI/mainwidget.cpp" line="1201"/>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1419"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="33"/>
        <source>Menu</source>
        <translation>ᠵᠠᠭᠤᠰᠢ ᠶᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ᠃</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">ᠰᠣᠩᠭᠣᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="45"/>
        <source>Theme</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="47"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="150"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="49"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="148"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="146"/>
        <source>Exit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="58"/>
        <source>Auto</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠢ᠋ ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="63"/>
        <source>Light</source>
        <translation>ᠭᠦᠶᠦᠬᠡᠨ ᠦᠨᠭᠭᠡ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="68"/>
        <source>Dark</source>
        <translation>ᠭᠦᠨ ᠦᠨᠭᠭᠡ</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="205"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="237"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="278"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠦᠨ</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation type="vanished">ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠨᠢ ᠡᠯ᠎ᠡ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠮᠸᠳᠢᠶᠠ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠰᠤᠹᠲ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠲᠡᠷᠡ ᠨᠢ ᠡᠯ᠎ᠡ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢ ᠬᠠᠮᠤᠷᠤᠭᠰᠠᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠪᠠᠢᠵᠤ᠂ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠨᠢ ᠳᠦᠷᠬᠡᠨ ᠳᠦᠬᠦᠮ ᠪᠠᠢᠳᠠᠭ.</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="206"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="282"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠄ </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">service &amp; support: </translation>
    </message>
    <message>
        <source>kylin-music</source>
        <translation type="vanished">ᠵᠢᠢ ᠯᠢᠨ ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠃</translation>
    </message>
</context>
<context>
    <name>miniWidget</name>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="54"/>
        <source>kylin-music mini</source>
        <translation type="unfinished">ᠵᠢᠢ ᠯᠢᠨ ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="70"/>
        <location filename="../UI/player/miniwidget.cpp" line="71"/>
        <location filename="../UI/player/miniwidget.cpp" line="447"/>
        <location filename="../UI/player/miniwidget.cpp" line="849"/>
        <source>Loop</source>
        <translation type="unfinished">ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠶ᠋ᠢᠨ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="72"/>
        <location filename="../UI/player/miniwidget.cpp" line="455"/>
        <location filename="../UI/player/miniwidget.cpp" line="854"/>
        <source>Random</source>
        <translation type="unfinished">ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="73"/>
        <source>Sequence</source>
        <translation type="unfinished">ᠳᠠᠷᠠᠭᠠᠯᠠᠯ&#x202f;ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="74"/>
        <location filename="../UI/player/miniwidget.cpp" line="439"/>
        <location filename="../UI/player/miniwidget.cpp" line="844"/>
        <source>CurrentItemInLoop</source>
        <translation type="unfinished">ᠳᠠᠨᠭ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="218"/>
        <location filename="../UI/player/miniwidget.cpp" line="282"/>
        <location filename="../UI/player/miniwidget.cpp" line="578"/>
        <source>Pause</source>
        <translation type="unfinished">ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="231"/>
        <location filename="../UI/player/miniwidget.cpp" line="244"/>
        <location filename="../UI/player/miniwidget.cpp" line="295"/>
        <location filename="../UI/player/miniwidget.cpp" line="308"/>
        <location filename="../UI/player/miniwidget.cpp" line="587"/>
        <source>Play</source>
        <translation type="unfinished">ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="374"/>
        <location filename="../UI/player/miniwidget.cpp" line="739"/>
        <location filename="../UI/player/miniwidget.cpp" line="866"/>
        <source>Music Player</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="375"/>
        <source>00:00/00:00</source>
        <translation type="unfinished">00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="402"/>
        <source>Previous</source>
        <translation type="unfinished">ᠳᠡᠭᠡᠷᠡᠬᠢ ᠲᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="411"/>
        <source>Next</source>
        <translation type="unfinished">ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="426"/>
        <source>Favourite</source>
        <translation type="unfinished">ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="472"/>
        <source>Close</source>
        <translation type="unfinished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="484"/>
        <source>normal mode</source>
        <translation type="unfinished">ᠬᠡᠪ ᠦᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="634"/>
        <location filename="../UI/player/miniwidget.cpp" line="650"/>
        <source>I Love</source>
        <translation type="unfinished">ᠪᠢ ᠬᠠᠢ᠌ᠷᠠᠲᠠᠢ ᠃</translation>
    </message>
</context>
</TS>
