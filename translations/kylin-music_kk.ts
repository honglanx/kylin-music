<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>AllPupWindow</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <location filename="../UI/base/allpupwindow.cpp" line="87"/>
        <source>Prompt information</source>
        <translation>ەسكەرتپە حابارلارى</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Font limit exceeded</source>
        <translation type="vanished">超出字数限制</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>ٶز ىشىنە المايتىن تومەندەگٸ ٴارىپتەر: / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="53"/>
        <source>Reached upper character limit</source>
        <translation>ٴارىپ سانى جوعارعى شەككە جەتى</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="62"/>
        <source>Music Player</source>
        <translation>مۇزيكا</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="115"/>
        <source>Confirm</source>
        <translation>تۇراقتاندىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="119"/>
        <source>Cancel</source>
        <translation>كۇشىنەن قالدىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="128"/>
        <source>Please input playlist name:</source>
        <translation>قويۋ تىزىمدىگنىڭ اتاعىن كىرگىزىڭىز:</translation>
    </message>
    <message>
        <source>Please enter the title of the playlist:</source>
        <translation type="vanished">请输入歌单标题：</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="229"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="265"/>
        <source>Unknown singer</source>
        <translation>كۋالىك ناخشىچى</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="230"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="268"/>
        <source>Unknown album</source>
        <translation>كۋالىك پلاستينكا</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Prompt information</source>
        <translation>ەسكەرتپە حابارلارى</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Add failed, no valid music file found</source>
        <translation>قوسۋ جەڭىلىس بولۋ،كۇشكە يە ەمەس مۇزيكا حۇجاتى تابىلمادى</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="299"/>
        <source>  Music Information</source>
        <translation>  مۇزيكا حابارى</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="240"/>
        <source>Music Player</source>
        <translation>مۇزيكا</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="387"/>
        <source>Confirm</source>
        <translation>تۇراقتاندىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="389"/>
        <source>Cancel</source>
        <translation>كۇشىنەن قالدىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="312"/>
        <source>Song Name : </source>
        <translation>ٴان اتٸ : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="313"/>
        <source>Singer : </source>
        <translation>ناخشىچى : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="314"/>
        <source>Album : </source>
        <translation>ئالبۇم : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="315"/>
        <source>File Type : </source>
        <translation>حۇجات تۇرى : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="316"/>
        <source>File Size : </source>
        <translation>حۇجات ۇلكەندىگى : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="317"/>
        <source>File Time : </source>
        <translation>حۇجات ۋاقىتى : </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="318"/>
        <source>File Path : </source>
        <translation>حۇجات جولى : </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="7"/>
        <source>Song</source>
        <translation>ٴان</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Singer</source>
        <translation>ناخشىچى</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Album</source>
        <translation>ئالبۇم</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Time</source>
        <translation>ۋاقىتىندا</translation>
    </message>
</context>
<context>
    <name>MyToolButton</name>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="142"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="155"/>
        <source>Play</source>
        <translation>قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="143"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="159"/>
        <source>Pause</source>
        <translation>توقتاتۋ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="144"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="163"/>
        <source>Rename</source>
        <translation>قاتە ات فاميليا ەتۋ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="145"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="167"/>
        <source>Delete</source>
        <translation>ٴوشىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>Song List</source>
        <translation>ٴان تٸزٸمدٸگٸ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>I Love</source>
        <translation>مەن جاقسى كۆرىمەن</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>داۋىرلىك ايلاما قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>ەركىن قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>تارتىپ بويىنشا قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>تاق مۇزيكا ايلاما قويۋ</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="32"/>
        <source>Previous</source>
        <translation>الداعٸ شاراپات</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="41"/>
        <source>Next</source>
        <translation>كەيىنگى شاراپات</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="45"/>
        <source>Volume</source>
        <translation>اۋا</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="106"/>
        <source>Favourite</source>
        <translation>جاقسى كورۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="133"/>
        <location filename="../UI/player/playsongarea.cpp" line="149"/>
        <location filename="../UI/player/playsongarea.cpp" line="486"/>
        <location filename="../UI/player/playsongarea.cpp" line="532"/>
        <source>Loop</source>
        <translation>داۋىرلىك ايلاما قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="168"/>
        <source>Play List</source>
        <translation>قويۋ تاريحى</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="188"/>
        <location filename="../UI/player/playsongarea.cpp" line="558"/>
        <location filename="../UI/player/playsongarea.cpp" line="560"/>
        <location filename="../UI/player/playsongarea.cpp" line="673"/>
        <location filename="../UI/player/playsongarea.cpp" line="906"/>
        <location filename="../UI/player/playsongarea.cpp" line="934"/>
        <source>Music Player</source>
        <translation>مۇزيكا</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="189"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="429"/>
        <location filename="../UI/player/playsongarea.cpp" line="446"/>
        <source>I Love</source>
        <translation>مەن جاقسى كۆرىمەن</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="607"/>
        <location filename="../UI/player/playsongarea.cpp" line="977"/>
        <location filename="../UI/player/playsongarea.cpp" line="1033"/>
        <source>Pause</source>
        <translation type="unfinished">توقتاتۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="617"/>
        <location filename="../UI/player/playsongarea.cpp" line="989"/>
        <location filename="../UI/player/playsongarea.cpp" line="1001"/>
        <location filename="../UI/player/playsongarea.cpp" line="1045"/>
        <location filename="../UI/player/playsongarea.cpp" line="1057"/>
        <source>Play</source>
        <translation type="unfinished">قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="141"/>
        <location filename="../UI/player/playsongarea.cpp" line="151"/>
        <location filename="../UI/player/playsongarea.cpp" line="495"/>
        <location filename="../UI/player/playsongarea.cpp" line="537"/>
        <source>Random</source>
        <translation>ەركىن قويۋ</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="125"/>
        <location filename="../UI/player/playsongarea.cpp" line="153"/>
        <location filename="../UI/player/playsongarea.cpp" line="512"/>
        <location filename="../UI/player/playsongarea.cpp" line="522"/>
        <source>CurrentItemInLoop</source>
        <translation>تاق مۇزيكا ايلاما قويۋ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="64"/>
        <source>Database Error</source>
        <translation>ساندىق قاتەلىگى</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="120"/>
        <source>Search Result</source>
        <translation>ٸزدەۋ ناتيجەسى</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="34"/>
        <source>Music</source>
        <translation>ٴان-مۇزيكا</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="50"/>
        <source>Singer</source>
        <translation>ناخشىچى</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="66"/>
        <source>Album</source>
        <translation>ئالبۇم</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="58"/>
        <source>Music Player</source>
        <translation>مۇزيكا</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="88"/>
        <source>Library</source>
        <translation>مۇزيكا قامباسٸ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="97"/>
        <source>Song List</source>
        <translation>ٴان تٸزٸمدٸگٸ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="109"/>
        <source>My PlayList</source>
        <translation>قويۋ تىزمدگىم</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="154"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="363"/>
        <source>New Playlist</source>
        <translation>جاڭا قويىلۋ تٸزٸمدٸگٸ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="158"/>
        <source>Rename</source>
        <translation>قاتە ات فاميليا ەتۋ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="268"/>
        <source>I Love</source>
        <translation>مەن جاقسى كۆرىمەن</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Prompt information</source>
        <translation>ەسكەرتپە حابارلارى</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Single song name already exists!!!</source>
        <translation>ٴان تٸزٸمدٸگٸ بۇل سويلەم ساقتالعان!!</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="44"/>
        <source>History</source>
        <translation>قويۋ تاريحى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="52"/>
        <source>Empty</source>
        <translation>قۇرعاق</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="70"/>
        <source>The playlist has no songs</source>
        <translation>مۇزيكا جوق</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <source>Prompt information</source>
        <translation>ەسكەرتپە حابارلارى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <source>Clear the playlist?</source>
        <translation>قويىلۋ تٸزٸمدٸكدٸ قۇرۇقدامسىز؟</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="131"/>
        <source>kylin-music</source>
        <translation>kylin-مۇزيكا</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="383"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="411"/>
        <source>path does not exist</source>
        <translation>جول ساقتالعان ەمەس</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="265"/>
        <source> songs</source>
        <translation> الدىداعى ٴان</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="294"/>
        <source>Play</source>
        <translation>قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="295"/>
        <source>Delete</source>
        <translation>ٴوشىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="296"/>
        <source>Play the next one</source>
        <translation>كەيىنگىسىن قويۋ</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="258"/>
        <location filename="../UI/tableview/tableone.cpp" line="1127"/>
        <location filename="../UI/tableview/tableone.cpp" line="1168"/>
        <location filename="../UI/tableview/tableone.cpp" line="1240"/>
        <source>Song List</source>
        <translation>ٴان تٸزٸمدٸگٸ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="191"/>
        <source>There are no songs!</source>
        <translation>مۇزيكا جوق</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="147"/>
        <source>Play All</source>
        <translation>ٴبارىن قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="150"/>
        <source>Add Music</source>
        <translation>مۇزيكا قوسۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="156"/>
        <source>Add local songs</source>
        <translation>اسبابٸتٸڭ وزىنىكى مۋزيكانى قوسۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <source>Add local folders</source>
        <translation>اسبابٸتٸڭ وزىنىكى حۇجاتتى قوسۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="192"/>
        <source>Add Local Songs</source>
        <translation>اسبابٸتٸڭ وزىنىكى مۋزيكانى قوسۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="193"/>
        <source>Add Local Folder</source>
        <translation>اسبابٸتٸڭ وزىنىكى حۇجاتتاردى قوسۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="260"/>
        <location filename="../UI/tableview/tableone.cpp" line="343"/>
        <location filename="../UI/tableview/tableone.cpp" line="716"/>
        <location filename="../UI/tableview/tableone.cpp" line="1129"/>
        <location filename="../UI/tableview/tableone.cpp" line="1170"/>
        <source>I Love</source>
        <translation>مەن جاقسى كۆرىمەن</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="262"/>
        <location filename="../UI/tableview/tableone.cpp" line="1131"/>
        <location filename="../UI/tableview/tableone.cpp" line="1172"/>
        <location filename="../UI/tableview/tableone.cpp" line="1217"/>
        <location filename="../UI/tableview/tableone.cpp" line="1266"/>
        <location filename="../UI/tableview/tableone.cpp" line="1295"/>
        <source>Search Result</source>
        <translation>ٸزدەۋ ناتيجەسى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="290"/>
        <source>Play</source>
        <translation>قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="291"/>
        <source>Delete from list</source>
        <translation>تٸزٸمدەن ٴوشىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="292"/>
        <source>Remove from local</source>
        <translation>جەرلىكتەن شىعارىپ وتۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="367"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>تالدانعان ٴاندى ٴان تٸزٸمدٸگٸنەن ئۆچۈرۈلىدىغانلىقىنى كەسٸم جاسايسٸزبە؟</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="369"/>
        <location filename="../UI/tableview/tableone.cpp" line="529"/>
        <location filename="../UI/tableview/tableone.cpp" line="633"/>
        <location filename="../UI/tableview/tableone.cpp" line="746"/>
        <location filename="../UI/tableview/tableone.cpp" line="826"/>
        <location filename="../UI/tableview/tableone.cpp" line="955"/>
        <location filename="../UI/tableview/tableone.cpp" line="966"/>
        <location filename="../UI/tableview/tableone.cpp" line="980"/>
        <location filename="../UI/tableview/tableone.cpp" line="990"/>
        <source>kylin-music</source>
        <translation>kylin-مۇزيكا</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="372"/>
        <location filename="../UI/tableview/tableone.cpp" line="401"/>
        <location filename="../UI/tableview/tableone.cpp" line="530"/>
        <location filename="../UI/tableview/tableone.cpp" line="634"/>
        <location filename="../UI/tableview/tableone.cpp" line="967"/>
        <location filename="../UI/tableview/tableone.cpp" line="991"/>
        <source>Yes</source>
        <translation>سونداي</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="373"/>
        <location filename="../UI/tableview/tableone.cpp" line="402"/>
        <location filename="../UI/tableview/tableone.cpp" line="531"/>
        <location filename="../UI/tableview/tableone.cpp" line="635"/>
        <source>No</source>
        <translation>جوق</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="397"/>
        <location filename="../UI/tableview/tableone.cpp" line="527"/>
        <location filename="../UI/tableview/tableone.cpp" line="631"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>ٴان ٶشٸرٸلگەننەن كەيىن قالپىنا كەلتىرۋگە بولمايدى. شىنىندا ٶشٸرەسٸزبە؟</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="796"/>
        <source>Audio File</source>
        <translation>ۇن-سىن حۇجاتى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="945"/>
        <source>This format file is not supported</source>
        <translation>نۇ ٴپىشىم حۇجاتىن قولدامايدى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="989"/>
        <source>Repeat add</source>
        <translation>قوسۋدى قايتالاۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1028"/>
        <source>path does not exist</source>
        <translation>جول ساقتالعان ەمەس</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="954"/>
        <location filename="../UI/tableview/tableone.cpp" line="979"/>
        <source>Success add %1 songs</source>
        <translation>1%ٴاندى جەڭىسكە جەتۋ قوسۋ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="965"/>
        <source>Add failed</source>
        <translation>قوسۋ جەڭىلىپ قالدى</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="293"/>
        <source>View song information</source>
        <translation>ٴان حابارلارى</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="294"/>
        <source>Add to songlist</source>
        <translation>ٴان تىزىمدىگىنە قوس</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="367"/>
        <location filename="../UI/tableview/tableone.cpp" line="397"/>
        <location filename="../UI/tableview/tableone.cpp" line="527"/>
        <location filename="../UI/tableview/tableone.cpp" line="631"/>
        <location filename="../UI/tableview/tableone.cpp" line="954"/>
        <location filename="../UI/tableview/tableone.cpp" line="965"/>
        <location filename="../UI/tableview/tableone.cpp" line="979"/>
        <location filename="../UI/tableview/tableone.cpp" line="989"/>
        <source>Prompt information</source>
        <translation>ەسكەرتپە حابارلارى</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1065"/>
        <source> songs</source>
        <translation> الدىداعى ٴان</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="58"/>
        <source>back</source>
        <translation>شەگىنۋ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="70"/>
        <source>forward</source>
        <translation>ىلگەرلەۋ</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="79"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="94"/>
        <source>Not logged in</source>
        <translation>تىزىمدەتىلمەگەن</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="157"/>
        <source>mini model</source>
        <translation>ٸقشامدالعان پىشىنى</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="166"/>
        <source>To minimize the</source>
        <translation>كىشرەيتۋ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>ۇلكەيتۋ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="188"/>
        <source>close</source>
        <translation>تىعنداۋ</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="667"/>
        <source>Music Player</source>
        <translation>مۇزيكا</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="934"/>
        <location filename="../UI/mainwidget.cpp" line="1209"/>
        <source>reduction</source>
        <translation>قالپىنا قايتارۋ</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="940"/>
        <location filename="../UI/mainwidget.cpp" line="1201"/>
        <source>maximize</source>
        <translation>ۇلكەيتۋ</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1419"/>
        <source>Prompt information</source>
        <translation>ەسكەرتپە حابارلارى</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="33"/>
        <source>Menu</source>
        <translation>تٸزٸمدٸك</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="45"/>
        <source>Theme</source>
        <translation>سلوب</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="47"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="150"/>
        <source>Help</source>
        <translation>جاردەم</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="49"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="148"/>
        <source>About</source>
        <translation>جايىندا</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="146"/>
        <source>Exit</source>
        <translation>شىعۋ اۋٸزٸ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="58"/>
        <source>Auto</source>
        <translation>اۆتوماتتى</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="63"/>
        <source>Light</source>
        <translation>جەڭىل</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="68"/>
        <source>Dark</source>
        <translation>قانىق رەڭ</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="205"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="237"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="278"/>
        <source>Music Player</source>
        <translation>مۇزيكا</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation type="vanished">مۇزيكا قۇيعىش بولسا ٵر ٴتۇر پىشىندەگى مۇزىكىلارنى كوپ جاناما قويۋ دەتالىندا ٸستەتەدٸ. ول ٵر ٴتۇر مۇزيكا پىشىندەگى قويۋ جابدىقتارىن ٶز ىشىنە الدى، جوبالاۋى تەز ۋا جاي بولادٸ.</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="206"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="282"/>
        <source>Version: </source>
        <translation>باسىلىمى: </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">قىزىمەت &gt; قولداۋ: </translation>
    </message>
</context>
<context>
    <name>miniWidget</name>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="374"/>
        <location filename="../UI/player/miniwidget.cpp" line="739"/>
        <location filename="../UI/player/miniwidget.cpp" line="866"/>
        <source>Music Player</source>
        <translation>مۇزيكا</translation>
    </message>
    <message>
        <source>列表循环</source>
        <translation type="vanished">تٸزٸمدٸك بويىنشا قويۋ</translation>
    </message>
    <message>
        <source>随机播放</source>
        <translation type="vanished">ەرٸكتٸ قويۋ</translation>
    </message>
    <message>
        <source>顺序播放</source>
        <translation type="vanished">تارتىپ بويىنشا قويۋ</translation>
    </message>
    <message>
        <source>单曲循环</source>
        <translation type="vanished">تاق مۇزيكا تەرتىۋىدە قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="375"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="74"/>
        <location filename="../UI/player/miniwidget.cpp" line="439"/>
        <location filename="../UI/player/miniwidget.cpp" line="844"/>
        <source>CurrentItemInLoop</source>
        <translation>تاق مۇزيكا ايلاما قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="54"/>
        <source>kylin-music mini</source>
        <translation type="unfinished">kylin-ﻡۇﺰﻴﻛﺍ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="73"/>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="218"/>
        <location filename="../UI/player/miniwidget.cpp" line="282"/>
        <location filename="../UI/player/miniwidget.cpp" line="578"/>
        <source>Pause</source>
        <translation type="unfinished">توقتاتۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="231"/>
        <location filename="../UI/player/miniwidget.cpp" line="244"/>
        <location filename="../UI/player/miniwidget.cpp" line="295"/>
        <location filename="../UI/player/miniwidget.cpp" line="308"/>
        <location filename="../UI/player/miniwidget.cpp" line="587"/>
        <source>Play</source>
        <translation type="unfinished">قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="402"/>
        <source>Previous</source>
        <translation type="unfinished">الداعٸ شاراپات</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="411"/>
        <source>Next</source>
        <translation type="unfinished">كەيىنگى شاراپات</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="426"/>
        <source>Favourite</source>
        <translation type="unfinished">جاقسى كورۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="472"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="484"/>
        <source>normal mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="634"/>
        <location filename="../UI/player/miniwidget.cpp" line="650"/>
        <source>I Love</source>
        <translation>مەن جاقسى كۆرىمەن</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="70"/>
        <location filename="../UI/player/miniwidget.cpp" line="71"/>
        <location filename="../UI/player/miniwidget.cpp" line="447"/>
        <location filename="../UI/player/miniwidget.cpp" line="849"/>
        <source>Loop</source>
        <translation>داۋىرلىك ايلاما قويۋ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="72"/>
        <location filename="../UI/player/miniwidget.cpp" line="455"/>
        <location filename="../UI/player/miniwidget.cpp" line="854"/>
        <source>Random</source>
        <translation>ەركىن قويۋ</translation>
    </message>
</context>
</TS>
