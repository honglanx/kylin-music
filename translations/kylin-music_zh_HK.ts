<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>AllPupWindow</name>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <location filename="../UI/base/allpupwindow.cpp" line="87"/>
        <source>Prompt information</source>
        <translation type="unfinished">提示資訊</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="45"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation type="unfinished">不能包含以下字元：\ / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="53"/>
        <source>Reached upper character limit</source>
        <translation type="unfinished">已達到字元上限</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="62"/>
        <source>Music Player</source>
        <translation type="unfinished">音樂</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="115"/>
        <source>Confirm</source>
        <translation type="unfinished">確認</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="119"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="128"/>
        <source>Please input playlist name:</source>
        <translation type="unfinished">請輸入歌單名稱：</translation>
    </message>
</context>
<context>
    <name>CustomToolButton</name>
    <message>
        <source>Play</source>
        <translation type="vanished">播放</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">暫停</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">刪除</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation type="vanished">我喜歡</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <source>Music Player</source>
        <translation type="vanished">音樂</translation>
    </message>
    <message>
        <source>Loop</source>
        <translation type="vanished">列表迴圈</translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="vanished">隨機播放</translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation type="vanished">順序播放</translation>
    </message>
    <message>
        <source>00:00/00:00</source>
        <translation type="vanished">00:00/00:00</translation>
    </message>
    <message>
        <source>CurrentItemInLoop</source>
        <translation type="vanished">單曲迴圈</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">播放</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">暫停</translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation type="vanished">喜歡</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">關閉</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>I Love</source>
        <translation type="vanished">我喜歡</translation>
    </message>
</context>
<context>
    <name>MusicDataBase</name>
    <message>
        <source>Prompt information</source>
        <translation type="obsolete">提示資訊</translation>
    </message>
    <message>
        <source>Single song name is already exists</source>
        <translation type="obsolete">歌單名已存在</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="229"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="265"/>
        <source>Unknown singer</source>
        <translation>未知歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="230"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="268"/>
        <source>Unknown album</source>
        <translation>未知專輯</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="430"/>
        <source>Add failed, no valid music file found</source>
        <translation>添加失敗，找不到有效的音樂檔</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <source>Music Information</source>
        <translation type="vanished">歌曲資訊</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">確認</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="240"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="387"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="389"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="312"/>
        <source>Song Name : </source>
        <translation>歌曲名： </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="299"/>
        <source>  Music Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="313"/>
        <source>Singer : </source>
        <translation>歌手： </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="314"/>
        <source>Album : </source>
        <translation>專輯： </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="315"/>
        <source>File Type : </source>
        <translation>檔類型： </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="316"/>
        <source>File Size : </source>
        <translation>檔大小： </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="317"/>
        <source>File Time : </source>
        <translation>歌曲時長： </translation>
    </message>
    <message>
        <location filename="../UI/base/allpupwindow.cpp" line="318"/>
        <source>File Path : </source>
        <translation>檔路徑： </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="7"/>
        <source>Song</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Album</source>
        <translation>專輯</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Time</source>
        <translation>時長</translation>
    </message>
</context>
<context>
    <name>MyToolButton</name>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="142"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="155"/>
        <source>Play</source>
        <translation type="unfinished">播放</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="143"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="159"/>
        <source>Pause</source>
        <translation type="unfinished">暫停</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="144"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="163"/>
        <source>Rename</source>
        <translation type="unfinished">重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="145"/>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="167"/>
        <source>Delete</source>
        <translation type="unfinished">刪除</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>Song List</source>
        <translation type="unfinished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/mytoolbutton.cpp" line="173"/>
        <source>I Love</source>
        <translation type="unfinished">我喜歡</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>列表迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>隨機播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>順序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>單曲迴圈</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="32"/>
        <source>Previous</source>
        <translation>上一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="617"/>
        <location filename="../UI/player/playsongarea.cpp" line="989"/>
        <location filename="../UI/player/playsongarea.cpp" line="1001"/>
        <location filename="../UI/player/playsongarea.cpp" line="1045"/>
        <location filename="../UI/player/playsongarea.cpp" line="1057"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="41"/>
        <source>Next</source>
        <translation>下一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="45"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="106"/>
        <source>Favourite</source>
        <translation>喜歡</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="133"/>
        <location filename="../UI/player/playsongarea.cpp" line="149"/>
        <location filename="../UI/player/playsongarea.cpp" line="486"/>
        <location filename="../UI/player/playsongarea.cpp" line="532"/>
        <source>Loop</source>
        <translation>列表迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="168"/>
        <source>Play List</source>
        <translation>播放歷史</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="188"/>
        <location filename="../UI/player/playsongarea.cpp" line="558"/>
        <location filename="../UI/player/playsongarea.cpp" line="560"/>
        <location filename="../UI/player/playsongarea.cpp" line="673"/>
        <location filename="../UI/player/playsongarea.cpp" line="906"/>
        <location filename="../UI/player/playsongarea.cpp" line="934"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="189"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="429"/>
        <location filename="../UI/player/playsongarea.cpp" line="446"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="607"/>
        <location filename="../UI/player/playsongarea.cpp" line="977"/>
        <location filename="../UI/player/playsongarea.cpp" line="1033"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="141"/>
        <location filename="../UI/player/playsongarea.cpp" line="151"/>
        <location filename="../UI/player/playsongarea.cpp" line="495"/>
        <location filename="../UI/player/playsongarea.cpp" line="537"/>
        <source>Random</source>
        <translation>隨機播放</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">順序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="125"/>
        <location filename="../UI/player/playsongarea.cpp" line="153"/>
        <location filename="../UI/player/playsongarea.cpp" line="512"/>
        <location filename="../UI/player/playsongarea.cpp" line="522"/>
        <source>CurrentItemInLoop</source>
        <translation>單曲迴圈</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <source>Prompt information</source>
        <translation type="vanished">提示資訊</translation>
    </message>
    <message>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation type="vanished">不能包含以下字元：\ / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <source>Reached upper character limit</source>
        <translation type="vanished">已達到字元上限</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">音樂</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">確認</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Please input playlist name:</source>
        <translation type="vanished">請輸入歌單名稱：</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="64"/>
        <source>Database Error</source>
        <translation>資料庫錯誤</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="120"/>
        <source>Search Result</source>
        <translation>搜索結果</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="34"/>
        <source>Music</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="50"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="66"/>
        <source>Album</source>
        <translation>專輯</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="58"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="88"/>
        <source>Library</source>
        <translation>音樂庫</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="97"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="109"/>
        <source>My PlayList</source>
        <translation>我的歌單</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="154"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="363"/>
        <source>New Playlist</source>
        <translation>新建歌單</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="158"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="268"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="322"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="412"/>
        <source>Single song name already exists!!!</source>
        <translation>歌單名已存在！</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放歷史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="44"/>
        <source>History</source>
        <translation>播放歷史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="52"/>
        <source>Empty</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="70"/>
        <source>The playlist has no songs</source>
        <translation>無音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="130"/>
        <source>Clear the playlist?</source>
        <translation>清空播放列表？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="131"/>
        <source>kylin-music</source>
        <translation type="unfinished">音樂</translation>
    </message>
    <message>
        <source>song</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <source>total</source>
        <translation type="vanished">共</translation>
    </message>
    <message>
        <source>songs</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="383"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="411"/>
        <source>path does not exist</source>
        <translation>路徑不存在</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="265"/>
        <source> songs</source>
        <translation> 首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="294"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="295"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="296"/>
        <source>Play the next one</source>
        <translation>下一首播放</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">專輯</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">時長</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="258"/>
        <location filename="../UI/tableview/tableone.cpp" line="1127"/>
        <location filename="../UI/tableview/tableone.cpp" line="1168"/>
        <location filename="../UI/tableview/tableone.cpp" line="1240"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="191"/>
        <source>There are no songs!</source>
        <translation>無音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="147"/>
        <source>Play All</source>
        <translation>播放全部</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="150"/>
        <source>Add Music</source>
        <translation>添加音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="156"/>
        <source>Add local songs</source>
        <translation>添加本地音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <source>Add local folders</source>
        <translation>添加本地檔夾</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="192"/>
        <source>Add Local Songs</source>
        <translation>添加本地音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="193"/>
        <source>Add Local Folder</source>
        <translation>添加本地檔夾</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="945"/>
        <source>This format file is not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">打開檔</translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation type="vanished">打開檔夾</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="260"/>
        <location filename="../UI/tableview/tableone.cpp" line="343"/>
        <location filename="../UI/tableview/tableone.cpp" line="716"/>
        <location filename="../UI/tableview/tableone.cpp" line="1129"/>
        <location filename="../UI/tableview/tableone.cpp" line="1170"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="262"/>
        <location filename="../UI/tableview/tableone.cpp" line="1131"/>
        <location filename="../UI/tableview/tableone.cpp" line="1172"/>
        <location filename="../UI/tableview/tableone.cpp" line="1217"/>
        <location filename="../UI/tableview/tableone.cpp" line="1266"/>
        <location filename="../UI/tableview/tableone.cpp" line="1295"/>
        <source>Search Result</source>
        <translation>搜索結果</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="290"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="291"/>
        <source>Delete from list</source>
        <translation>刪除選中項</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="292"/>
        <source>Remove from local</source>
        <translation>從本地刪除</translation>
    </message>
    <message>
        <source>Delete Selected Items</source>
        <translation type="vanished">刪除選中項</translation>
    </message>
    <message>
        <source>Song Information</source>
        <translation type="vanished">歌曲資訊</translation>
    </message>
    <message>
        <source>Add To Songlist</source>
        <translation type="vanished">添加歌曲到</translation>
    </message>
    <message>
        <source>Open The Folder</source>
        <translation type="vanished">打開所在檔夾</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="367"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>確認將選中的歌曲從歌單中刪除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="369"/>
        <location filename="../UI/tableview/tableone.cpp" line="529"/>
        <location filename="../UI/tableview/tableone.cpp" line="633"/>
        <location filename="../UI/tableview/tableone.cpp" line="746"/>
        <location filename="../UI/tableview/tableone.cpp" line="826"/>
        <location filename="../UI/tableview/tableone.cpp" line="955"/>
        <location filename="../UI/tableview/tableone.cpp" line="966"/>
        <location filename="../UI/tableview/tableone.cpp" line="980"/>
        <location filename="../UI/tableview/tableone.cpp" line="990"/>
        <source>kylin-music</source>
        <translation type="unfinished">音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="372"/>
        <location filename="../UI/tableview/tableone.cpp" line="401"/>
        <location filename="../UI/tableview/tableone.cpp" line="530"/>
        <location filename="../UI/tableview/tableone.cpp" line="634"/>
        <location filename="../UI/tableview/tableone.cpp" line="967"/>
        <location filename="../UI/tableview/tableone.cpp" line="991"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="373"/>
        <location filename="../UI/tableview/tableone.cpp" line="402"/>
        <location filename="../UI/tableview/tableone.cpp" line="531"/>
        <location filename="../UI/tableview/tableone.cpp" line="635"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="397"/>
        <location filename="../UI/tableview/tableone.cpp" line="527"/>
        <location filename="../UI/tableview/tableone.cpp" line="631"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>歌曲從本地刪除後不可恢復，是否確定刪除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="796"/>
        <source>Audio File</source>
        <translation>音頻檔</translation>
    </message>
    <message>
        <source>Are you sure to clear the list?</source>
        <translation type="vanished">確認清空歌單列表？</translation>
    </message>
    <message>
        <source>Add failed! Format error or no access permission!</source>
        <translation type="vanished">添加失敗！格式錯誤或無訪問許可權！</translation>
    </message>
    <message>
        <source>Success add %1 song</source>
        <translation type="vanished">成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="989"/>
        <source>Repeat add</source>
        <translation>重複添加</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1028"/>
        <source>path does not exist</source>
        <translation>路徑不存在</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您確定從本地刪除嗎？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="954"/>
        <location filename="../UI/tableview/tableone.cpp" line="979"/>
        <source>Success add %1 songs</source>
        <translation>成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="965"/>
        <source>Add failed</source>
        <translation>添加失敗</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">刪除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="293"/>
        <source>View song information</source>
        <translation>歌曲資訊</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="294"/>
        <source>Add to songlist</source>
        <translation>添加歌曲到</translation>
    </message>
    <message>
        <source>Clear List</source>
        <translation type="vanished">清空列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="367"/>
        <location filename="../UI/tableview/tableone.cpp" line="397"/>
        <location filename="../UI/tableview/tableone.cpp" line="527"/>
        <location filename="../UI/tableview/tableone.cpp" line="631"/>
        <location filename="../UI/tableview/tableone.cpp" line="954"/>
        <location filename="../UI/tableview/tableone.cpp" line="965"/>
        <location filename="../UI/tableview/tableone.cpp" line="979"/>
        <location filename="../UI/tableview/tableone.cpp" line="989"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失敗！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲檔失敗！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1065"/>
        <source> songs</source>
        <translation> 首歌曲</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="58"/>
        <source>back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="70"/>
        <source>forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音樂，歌手</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="79"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="94"/>
        <source>Not logged in</source>
        <translation>未登錄</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">主菜單</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">主菜單</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="157"/>
        <source>mini model</source>
        <translation>精簡模式</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="166"/>
        <source>To minimize the</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="188"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="667"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="934"/>
        <location filename="../UI/mainwidget.cpp" line="1209"/>
        <source>reduction</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="940"/>
        <location filename="../UI/mainwidget.cpp" line="1201"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1419"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="33"/>
        <source>Menu</source>
        <translation>菜單</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">選項</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">選項</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="45"/>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="47"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="150"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="49"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="148"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="146"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="58"/>
        <source>Auto</source>
        <translation>跟隨主題</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="63"/>
        <source>Light</source>
        <translation>淺色</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="68"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="205"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="237"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="278"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation type="vanished">音樂播放器是用於播放各種音樂格式的多媒體播放軟體。它是涵蓋了各種音樂格式的播放工具，操作快捷簡單。</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音樂播放器是一種用於播放各種音樂檔的多媒體播放軟體。它是涵蓋了各種音樂格式的播放工具，操作快捷簡單。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音樂播放器是一種用於播放各種音樂檔的多媒體播放軟體。它是涵蓋了各種音樂格式的播放工具，操作快捷簡單</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="206"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="282"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服務與支持團隊：</translation>
    </message>
    <message>
        <source>kylin-music</source>
        <translation type="vanished">音樂</translation>
    </message>
</context>
<context>
    <name>miniWidget</name>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="54"/>
        <source>kylin-music mini</source>
        <translation type="unfinished">音樂</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="70"/>
        <location filename="../UI/player/miniwidget.cpp" line="71"/>
        <location filename="../UI/player/miniwidget.cpp" line="447"/>
        <location filename="../UI/player/miniwidget.cpp" line="849"/>
        <source>Loop</source>
        <translation type="unfinished">列表迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="72"/>
        <location filename="../UI/player/miniwidget.cpp" line="455"/>
        <location filename="../UI/player/miniwidget.cpp" line="854"/>
        <source>Random</source>
        <translation type="unfinished">隨機播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="73"/>
        <source>Sequence</source>
        <translation type="unfinished">順序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="74"/>
        <location filename="../UI/player/miniwidget.cpp" line="439"/>
        <location filename="../UI/player/miniwidget.cpp" line="844"/>
        <source>CurrentItemInLoop</source>
        <translation type="unfinished">單曲迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="218"/>
        <location filename="../UI/player/miniwidget.cpp" line="282"/>
        <location filename="../UI/player/miniwidget.cpp" line="578"/>
        <source>Pause</source>
        <translation type="unfinished">暫停</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="231"/>
        <location filename="../UI/player/miniwidget.cpp" line="244"/>
        <location filename="../UI/player/miniwidget.cpp" line="295"/>
        <location filename="../UI/player/miniwidget.cpp" line="308"/>
        <location filename="../UI/player/miniwidget.cpp" line="587"/>
        <source>Play</source>
        <translation type="unfinished">播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="374"/>
        <location filename="../UI/player/miniwidget.cpp" line="739"/>
        <location filename="../UI/player/miniwidget.cpp" line="866"/>
        <source>Music Player</source>
        <translation type="unfinished">音樂</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="375"/>
        <source>00:00/00:00</source>
        <translation type="unfinished">00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="402"/>
        <source>Previous</source>
        <translation type="unfinished">上一首</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="411"/>
        <source>Next</source>
        <translation type="unfinished">下一首</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="426"/>
        <source>Favourite</source>
        <translation type="unfinished">喜歡</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="472"/>
        <source>Close</source>
        <translation type="unfinished">關閉</translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="484"/>
        <source>normal mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniwidget.cpp" line="634"/>
        <location filename="../UI/player/miniwidget.cpp" line="650"/>
        <source>I Love</source>
        <translation type="unfinished">我喜歡</translation>
    </message>
</context>
</TS>
